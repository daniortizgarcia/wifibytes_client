import {changeLang} from '../src/i18n/translate'
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<label id="lang" data-lang="Es"></lang>';
  
});

test('Test function changeLang',function(){
    changeLang("es");
    expect($('#lang').text()).toBe("Es")
    changeLang("en");
    expect($('#lang').text()).toBe("Sp")
})