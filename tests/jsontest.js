let dataJumbotron = [{
    "key": "juMBotrOn- 4",
    "content": "<p><strong style=\"margin: 0px; padding: 0px; font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>",
    "image": "http://localhost:8000/media/info_empresa_image/Samsung_Plasma_TV_Demo_Full_HD_1080pyoutube.com.mp4",
    "lang": "es"
},
{
    "key": "jumbotron- 1",
    "content": "<p><strong style=\"margin: 0px; padding: 0px; font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>",
    "image": "http://localhost:8000/media/info_empresa_image/slider1.jpg",
    "lang": "es"
},
{
    "key": "jumbotron - 2 ",
    "content": "<p><strong style=\"margin: 0px; padding: 0px; font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>",
    "image": "http://localhost:8000/media/info_empresa_image/slider2.jpg",
    "lang": "es"
},
{
    "key": "jumbotron - 3",
    "content": "<p><strong style=\"margin: 0px; padding: 0px; font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>",
    "image": "http://localhost:8000/media/info_empresa_image/slider3.jpg",
    "lang": "es"
}];
let dataContact = {address:"Avinguda Almaig",city:"Ontinyent",zipcode:"46870",province:"Valencia",country:"España",phone:"666666666"};
let dataTariffs = [{
    "codtarifa": 2,
    "nombretarifa": "CHICKEN",
    "slug": "chicken",
    "pretitulo": "CHICKEN",
    "logo": "http://localhost:8000/media/Logo/icon-chicken.png",
    "precio": 15.95,
    "activo": true,
    "destacado": true,
    "color": {
        "id": 2,
        "titulo": "Naranja",
        "hexadecimal": "fda100"
    },
    "subtarifas": [
        {
            "subtarifa_id": 4,
            "subtarifa_datos_internet": null,
            "subtarifa_cent_minuto": null,
            "subtarifa_est_llamada": null,
            "subtarifa_precio_sms": null,
            "subtarifa_minutos_gratis": null,
            "subtarifa_minutos_ilimitados": false,
            "subtarifa_velocidad_conexion_subida": 5.0,
            "subtarifa_velocidad_conexion_bajada": 9.0,
            "subtarifa_num_canales": null,
            "subtarifa_siglas_omv": "",
            "subtarifa_omv": null,
            "tipo_tarifa": 4,
            "subtarifa_tarifa": {
                "codtarifa": 2,
                "nombretarifa": "CHICKEN",
                "slug": "chicken",
                "pretitulo": "CHICKEN",
                "pretitulo_va": "CHICKEN",
                "logo": "media/Logo/icon-chicken.png",
                "precio": 15.95,
                "activo": true,
                "destacado": true,
                "created_at": 1540637879,
                "updated_at": 1541530766,
                "color": 2
            }
        }
    ]
},
{
    "codtarifa": 3,
    "nombretarifa": "PACK FIBRA",
    "slug": "pack-fibra",
    "pretitulo": "PACK FIBRA",
    "logo": "http://localhost:8000/media/Logo/logo-icon-wifibytes_iNeZjc9.png",
    "precio": 29.95,
    "activo": true,
    "destacado": false,
    "color": {
        "id": 3,
        "titulo": "Verde",
        "hexadecimal": "00b02e"
    }
}];
let home = [{
    "pk": 1,
    "titulo": "Texto Principal",
    "subtitulo": "Texto principal del Home",
    "caja_izquierda_titulo": "¿Por que Wicofi?",
    "caja_izquierda_texto": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">&iquest;Quieres darte de baja? &iquest;Cambiar &nbsp;de tarifa? &iquest; Activar el roaming? &iquest;Desactivar el buzon de voz? Sin problema. Entra en tu cuenta y hazlo con un solo click.Sin preguntas o llamadas a telefonos asistidos.</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">Simple y facil. Todo el soporte y atenci&oacute;n v&iacute;a internet con un sistema de tickets.</span></p>",
    "caja_derecha_titulo": "¿Por qué Wicofi?",
    "caja_derecha_texto": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">&iquest;Quieres darte de baja? &iquest;Cambiar &nbsp;de tarifa? &iquest; Activar el roaming? &iquest;Desactivar el buzon de voz? Sin problema. Entra en tu cuenta y hazlo con un solo click.Sin preguntas o llamadas a telefonos</span></p>",
    "activo": true,
    "idioma": 1,
    "lang": "es"
}];
let datos_empresa = {
    "name": "Wicofi",
    "cifnif": "",
    "phone": "666666666",
    "logo": "http://127.0.0.1:8000/media/logo/wicofi_M5GdrAv.svg",
    "icon_logo": "http://127.0.0.1:8000/media/icon_logo/wicofi.png",
    "mapa_cobertura": null,
    "address": "Calle Batalla de Lepanto",
    "city": "Bocairent",
    "province": "Valencia",
    "country": "España",
    "zipcode": "46880",
    "location_lat": "38.763887",
    "location_long": "-0.615433",
    "facebook": "https://es-es.facebook.com/wifibytes/",
    "twitter": "https://twitter.com/wifibyte_es?lang=es",
    "textos": [
        {
            "key": "cookies - en",
            "content": "<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">Esto es ingles xavalPol&iacute;tica de Cookies en Wifibytes</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Nuestro sitio web&nbsp;</span><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"http://wifibytes-front.wearecactus.com/#/\"><span style=\"box-sizing: border-box; margin: 0px auto; color: #3d3d3c;\">http://wifibytes-front.wearecactus.com/#/</span></a><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;utiliza una tecnolog&iacute;a denominada &ldquo;cookies&rdquo;, con la finalidad de poder recabar informaci&oacute;n acerca del uso del sitio web.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Le informamos que vamos a utilizar cookies con la finalidad de facilitar su navegaci&oacute;n a trav&eacute;s del sitio Web, distinguirle de otros usuarios, proporcionarle una mejor experiencia en el uso del mismo, e identificar problemas para mejorar nuestro Sitio Web. Asimismo, en caso de que preste su consentimiento a trav&eacute;s de su navegaci&oacute;n, utilizaremos cookies que nos permitan obtener m&aacute;s informaci&oacute;n acerca de sus preferencias y personalizar nuestro sitio Web de conformidad con sus intereses individuales.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">La presente Pol&iacute;tica de Cookies tiene por finalidad informarle de manera clara y precisa sobre las cookies que se utilizan en el sitio Web. En caso de que quiera recabar m&aacute;s informaci&oacute;n sobre las cookies que utilizamos en el sitio Web, podr&aacute; remitir un correo electr&oacute;nico a la siguiente direcci&oacute;n:&nbsp;</span><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"mailto:info@wifibytes.com\"><span style=\"box-sizing: border-box; margin: 0px auto; color: #3d3d3c;\">info@wifibytes.com</span></a></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">&iquest;Qu&eacute; son las cookies?</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Una cookie es un peque&ntilde;o fragmento de texto que los sitios web que visita env&iacute;an al navegador y que permite que el sitio web recuerde informaci&oacute;n sobre su visita, idioma preferido y otras opciones, lo que puede facilitar su pr&oacute;xima visita y hacer que el sitio le resulte m&aacute;s &uacute;til. Las cookies desempe&ntilde;an un papel muy importante y contribuyen a tener una mejor experiencia de usuario.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">&iquest;Por qu&eacute; son importantes?</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Las cookies nos permiten hacer del sitio Web de Wifibytes un lugar mejor, m&aacute;s r&aacute;pido y m&aacute;s seguro. Las cookies nos ayudan, por ejemplo, a:</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">&nbsp;</p>\r\n<ul style=\"box-sizing: border-box; margin: 0px auto 10px; color: #3d3d3c; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px;\">\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Habilitar ciertas funciones.</span></p>\r\n</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Proporcionar una experiencia de navegaci&oacute;n m&aacute;s personalizada.</span></p>\r\n</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Proteger la seguridad de tu cuenta.</span></p>\r\n</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Investigar, mejorar y comprender el uso que reciben nuestros productos y servicios.</span>&nbsp;</p>\r\n</li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">&iquest;Qu&eacute; tipos de Cookies utiliza Wifibytes</span>?</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Cookies de car&aacute;cter t&eacute;cnico y de personalizaci&oacute;n:</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Son aqu&eacute;llas que nos permiten controlar aspectos relacionados con la sesi&oacute;n del usuario, de tal forma que podemos mejorar la experiencia de navegaci&oacute;n dentro del portal. Por ejemplo:</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">&nbsp;</p>\r\n<ul style=\"box-sizing: border-box; margin: 0px auto 10px; color: #3d3d3c; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px;\">\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Nos permiten reconocer el idioma del usuario.</span></p>\r\n</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Impiden o dificultan ataques contra el sitio web o sus usuarios.</span></p>\r\n</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Nos ayudan a proporcionar la correcta reproducci&oacute;n de los contenidos multimedia.</span></p>\r\n</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Mejoran el rendimiento, permitiendo distribuir el tr&aacute;fico web de nuestras m&aacute;quinas entre varios servidores.</span></p>\r\n</li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">&iquest;Qui&eacute;n utiliza las cookies de Wifibytes?</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">A continuaci&oacute;n se muestra el listado de empresas (Terceros), que hacen uso de las cookies:</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\"><span style=\"box-sizing: border-box; margin: 0px auto;\"><span style=\"box-sizing: border-box; margin: 0px auto;\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Google Analytics</span><span style=\"box-sizing: border-box; margin: 0px auto;\">, permiten cuantificar el n&uacute;mero de visitantes y analizar estad&iacute;sticamente la utilizaci&oacute;n que hacen los usuarios de nuestros servicios.</span></span></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">Consentimiento</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Al navegar y continuar en el sitio Web estar&aacute; consintiendo el uso de las cookies antes enunciadas, por los plazos se&ntilde;alados y en las condiciones contenidas en la presente Pol&iacute;tica de Cookies.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Puedes obtener m&aacute;s informaci&oacute;n sobre nosotros a trav&eacute;s de nuestro&nbsp;</span><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"http://wifibytes-front.wearecactus.com/#/avisolegal\"><span style=\"box-sizing: border-box; margin: 0px auto; color: #3d3d3c;\">aviso legal,</span></a><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;nuestra pol&iacute;tica de privacidad, o mediante correo electr&oacute;nico dirigido a la direcci&oacute;n:&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\"><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"mailto:info@wifibytes.com\">info@wifibytes.com</a></span></p>",
            "image": null,
            "lang": "en"
        },
        {
            "key": "aviso legal - en",
            "content": "<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">Esto es ingles xaval DATOS GENERALES</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">De acuerdo con el art&iacute;culo 10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Informaci&oacute;n y de Comercio Electr&oacute;nico ponemos a su disposici&oacute;n los siguientes datos:</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Wifibytes SL. est&aacute; domiciliada en la calle C/Batalla de Lepanto n&ordm;5, Bajo (Bocairent), con CIF B98137078.Inscrita en el Registro Mercantil de Valencia, Vol .9030 , Folio 34, Hoja V-.133779, Inscripci&oacute;n 4&ordf;.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">En la web&nbsp;</span><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"http://wifibytes-front.wearecactus.com/#/\"><span style=\"box-sizing: border-box; margin: 0px auto; color: #3d3d3c;\">http://wifibytes-front.wearecactus.com/#/</span></a><span style=\"box-sizing: border-box; margin: 0px auto;\">hay una serie de contenidos de car&aacute;cter informativo sobre&nbsp;</span><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"http://wifibytes-front.wearecactus.com/#/nosotros\"><span style=\"box-sizing: border-box; margin: 0px auto; color: #3d3d3c;\">http://wifibytes-front.wearecactus.com/#/nosotros</span></a></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Su principal objetivo</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;es facilitar a los clientes y al p&uacute;blico en general, la informaci&oacute;n relativa a la empresa, a los productos y servicios que se ofrecen&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">POL&Iacute;TICA DE PRIVACIDAD</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">En cumplimiento de lo dispuesto en&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">la Ley Org&aacute;nica 15/1999, de 13 de Diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal (LOPD)&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">se informa al usuario que todos los datos que nos proporcione ser&aacute;n incorporados a un fichero, creado y mantenido bajo la responsabilidad de Wifibytes SL</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Siempre</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;se va a respetar la confidencialidad de sus datos personales&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">que s&oacute;lo ser&aacute;n utilizados con la finalidad de gestionar los servicios ofrecidos, atender a las solicitudes que nos plantee, realizar tareas administrativas, as&iacute; como remitir informaci&oacute;n t&eacute;cnica, comercial o publicitaria por v&iacute;a ordinaria o electr&oacute;nica.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Para ejercer sus&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">derechos de oposici&oacute;n, rectificaci&oacute;n o cancelaci&oacute;n&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">deber&aacute; dirigirse a la sede de la empresa C/Batalla de Lepanto, n&ordm;5 Bajo (Bocairent), escribirnos al siguiente correo info@wifibytes.com o ll&aacute;manos al 960 500 606</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">CONDICIONES DE USO</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Las condiciones de acceso y uso del presente sitio web se rigen por la legalidad vigente y por el principio de buena fe</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;comprometi&eacute;ndose el usuario a realizar un buen uso de la web. No se permiten conductas que vayan contra la ley, los derechos o intereses de terceros.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Ser usuario de la web de&nbsp;</span><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"http://web.wifibytes.com/front/#/\"><span style=\"box-sizing: border-box; margin: 0px auto; color: #3d3d3c;\">http://wifibytes.wearecactus.com/front/#/</span></a><span style=\"box-sizing: border-box; margin: 0px auto;\">implica que reconoce haber le&iacute;do y aceptado las presentes condiciones y lo que las extienda la normativa legal aplicable en esta materia. Si por el motivo que fuere no est&aacute; de acuerdo con estas condiciones no contin&uacute;e usando esta web.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Cualquier tipo de notificaci&oacute;n y/o reclamaci&oacute;n solamente ser&aacute; v&aacute;lida por notificaci&oacute;n escrita y/o correo certificado.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">RESPONSABILIDADES</span></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Wifibytes SL no se hace responsable de la informaci&oacute;n y contenidos almacenados en foros, redes sociales o cualesquier otro medio</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;que permita a terceros publicar contenidos de forma independiente en la p&aacute;gina web del prestador.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Sin embargo, teniendo en cuenta los art. 11 y 16 de la LSSI-CE,Wifibytes SL&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">se compromete a la retirada o en su caso bloqueo de aquellos contenidos que pudieran afectar o contravenir la legislaci&oacute;n nacional o internacional</span><span style=\"box-sizing: border-box; margin: 0px auto;\">, derechos de terceros o la moral y el orden p&uacute;blico.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Tampoco la empresa se responsabilizar&aacute; de los da&ntilde;os y perjuicios que se produzcan por fallos o malas configuraciones del software instalado en el ordenador del internauta.</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;Se excluye toda responsabilidad por alguna incidencia t&eacute;cnica o fallo que se produzca cuando el usuario se conecte a internet. Igualmente no se garantiza la inexistencia de interrupciones o errores en el acceso al sitio web.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">As&iacute; mismo Wifibytes SL&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">se reserva el derecho a actualizar, modificar o eliminar la informaci&oacute;n contenida en su p&aacute;gina web</span><span style=\"box-sizing: border-box; margin: 0px auto;\">, as&iacute; como la configuraci&oacute;n o presentaci&oacute;n del mismo, en cualquier momento sin asumir alguna responsabilidad por ello.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Le comunicamos que cualquier precio que pueda ver en nuestra web ser&aacute; solamente orientativo. Si el usuario desea saber con exactitud el precio o si el producto en el momento actual cuenta con alguna oferta de la cual se puede beneficiar ha de acudir a alguna de las tiendas f&iacute;sicas con las que cuenta Wifibytes SL.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">PROPIEDAD INTELECTUAL E INDUSTRIAL</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Wifibytes SL.</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;es titular de todos los derechos sobre el software de la publicaci&oacute;n digital as&iacute; como de los derechos de propiedad industrial e intelectual referidos a los contenidos que se incluyan</span><span style=\"box-sizing: border-box; margin: 0px auto;\">, a excepci&oacute;n de los derechos sobre productos y servicios de car&aacute;cter p&uacute;blico que no son propiedad de esta empresa.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Ning&uacute;n material publicado en esta web podr&aacute; ser reproducido, copiado o publicado</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;sin el consentimiento por escrito de Wifibytes SL.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Toda la informaci&oacute;n que se reciba en la web, como comentarios, sugerencias o ideas, se considerar&aacute; cedida</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;a Wifibytes SL.de manera gratuita. No debe enviarse informaci&oacute;n que NO pueda ser tratada de este modo.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Todos los productos y servicios de estas p&aacute;ginas que NO son propiedad de Wifibytes SL. son marcas registradas de sus respectivos propietarios y son reconocidas como tales por nuestra empresa. Solamente aparecen en la web de Wifibytes SL. a efectos de promoci&oacute;n y de recopilaci&oacute;n de informaci&oacute;n. Estos propietarios pueden solicitar la modificaci&oacute;n o eliminaci&oacute;n de la informaci&oacute;n que les pertenece.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">LEY APLICABLE Y JURISDICCI&Oacute;N</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Las presentes condiciones generales se rigen por la legislaci&oacute;n espa&ntilde;ola.</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;Para cualquier litigio que pudiera surgir relacionado con el sitio web o la actividad que en &eacute;l se desarrolla ser&aacute;n competentes Juzgados de Ontinyent, renunciando expresamente el usuario a cualquier otro fuero que pudiera corresponderle.</span></p>",
            "image": null,
            "lang": "en"
        },
        {
            "key": "nosotros - en",
            "content": "<p><span style=\"font-family: 'Times New Roman'; font-size: medium;\">Esto es ingles eh, Lorem Ipsum</span><span style=\"margin: 0px; padding: 0px; text-align: justify; font-family: 'Open Sans', Arial, sans-serif; font-size: 14px;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span></p>",
            "image": null,
            "lang": "en"
        },
        {
            "key": "nosotros - 1",
            "content": "<p><span style=\"font-family: 'Times New Roman'; font-size: medium;\">Lorem Ipsum</span><span style=\"margin: 0px; padding: 0px; text-align: justify; font-family: 'Open Sans', Arial, sans-serif; font-size: 14px;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span></p>",
            "image": null,
            "lang": "es"
        },
        {
            "key": "nosotros - 2",
            "content": "<p>Lorem Ipsum<span style=\"font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span></p>",
            "image": null,
            "lang": "es"
        },
        {
            "key": "juMBotrOn- 4",
            "content": "<p><strong style=\"margin: 0px; padding: 0px; font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>",
            "image": "http://localhost:8000/media/info_empresa_image/Samsung_Plasma_TV_Demo_Full_HD_1080pyoutube.com.mp4",
            "lang": "es"
        },
        {
            "key": "jumbotron- 1",
            "content": "<p><strong style=\"margin: 0px; padding: 0px; font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>",
            "image": "http://localhost:8000/media/info_empresa_image/slider1.jpg",
            "lang": "es"
        },
        {
            "key": "jumbotron - 2 ",
            "content": "<p><strong style=\"margin: 0px; padding: 0px; font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>",
            "image": "http://localhost:8000/media/info_empresa_image/slider2.jpg",
            "lang": "es"
        },
        {
            "key": "jumbotron - 3",
            "content": "<p><strong style=\"margin: 0px; padding: 0px; font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: 'Open Sans', Arial, sans-serif; font-size: 14px; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>",
            "image": "http://localhost:8000/media/info_empresa_image/slider3.jpg",
            "lang": "es"
        },
        {
            "key": "cookies",
            "content": "<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">Pol&iacute;tica de Cookies en Wifibytes</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Nuestro sitio web&nbsp;</span><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"http://wifibytes-front.wearecactus.com/#/\"><span style=\"box-sizing: border-box; margin: 0px auto; color: #3d3d3c;\">http://wifibytes-front.wearecactus.com/#/</span></a><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;utiliza una tecnolog&iacute;a denominada &ldquo;cookies&rdquo;, con la finalidad de poder recabar informaci&oacute;n acerca del uso del sitio web.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Le informamos que vamos a utilizar cookies con la finalidad de facilitar su navegaci&oacute;n a trav&eacute;s del sitio Web, distinguirle de otros usuarios, proporcionarle una mejor experiencia en el uso del mismo, e identificar problemas para mejorar nuestro Sitio Web. Asimismo, en caso de que preste su consentimiento a trav&eacute;s de su navegaci&oacute;n, utilizaremos cookies que nos permitan obtener m&aacute;s informaci&oacute;n acerca de sus preferencias y personalizar nuestro sitio Web de conformidad con sus intereses individuales.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">La presente Pol&iacute;tica de Cookies tiene por finalidad informarle de manera clara y precisa sobre las cookies que se utilizan en el sitio Web. En caso de que quiera recabar m&aacute;s informaci&oacute;n sobre las cookies que utilizamos en el sitio Web, podr&aacute; remitir un correo electr&oacute;nico a la siguiente direcci&oacute;n:&nbsp;</span><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"mailto:info@wifibytes.com\"><span style=\"box-sizing: border-box; margin: 0px auto; color: #3d3d3c;\">info@wifibytes.com</span></a></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">&iquest;Qu&eacute; son las cookies?</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Una cookie es un peque&ntilde;o fragmento de texto que los sitios web que visita env&iacute;an al navegador y que permite que el sitio web recuerde informaci&oacute;n sobre su visita, idioma preferido y otras opciones, lo que puede facilitar su pr&oacute;xima visita y hacer que el sitio le resulte m&aacute;s &uacute;til. Las cookies desempe&ntilde;an un papel muy importante y contribuyen a tener una mejor experiencia de usuario.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">&iquest;Por qu&eacute; son importantes?</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Las cookies nos permiten hacer del sitio Web de Wifibytes un lugar mejor, m&aacute;s r&aacute;pido y m&aacute;s seguro. Las cookies nos ayudan, por ejemplo, a:</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">&nbsp;</p>\r\n<ul style=\"box-sizing: border-box; margin: 0px auto 10px; color: #3d3d3c; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px;\">\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Habilitar ciertas funciones.</span></p>\r\n</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Proporcionar una experiencia de navegaci&oacute;n m&aacute;s personalizada.</span></p>\r\n</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Proteger la seguridad de tu cuenta.</span></p>\r\n</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Investigar, mejorar y comprender el uso que reciben nuestros productos y servicios.</span>&nbsp;</p>\r\n</li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">&iquest;Qu&eacute; tipos de Cookies utiliza Wifibytes</span>?</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Cookies de car&aacute;cter t&eacute;cnico y de personalizaci&oacute;n:</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Son aqu&eacute;llas que nos permiten controlar aspectos relacionados con la sesi&oacute;n del usuario, de tal forma que podemos mejorar la experiencia de navegaci&oacute;n dentro del portal. Por ejemplo:</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">&nbsp;</p>\r\n<ul style=\"box-sizing: border-box; margin: 0px auto 10px; color: #3d3d3c; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px;\">\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Nos permiten reconocer el idioma del usuario.</span></p>\r\n</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Impiden o dificultan ataques contra el sitio web o sus usuarios.</span></p>\r\n</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Nos ayudan a proporcionar la correcta reproducci&oacute;n de los contenidos multimedia.</span></p>\r\n</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">- Mejoran el rendimiento, permitiendo distribuir el tr&aacute;fico web de nuestras m&aacute;quinas entre varios servidores.</span></p>\r\n</li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">&iquest;Qui&eacute;n utiliza las cookies de Wifibytes?</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">A continuaci&oacute;n se muestra el listado de empresas (Terceros), que hacen uso de las cookies:</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\"><span style=\"box-sizing: border-box; margin: 0px auto;\"><span style=\"box-sizing: border-box; margin: 0px auto;\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Google Analytics</span><span style=\"box-sizing: border-box; margin: 0px auto;\">, permiten cuantificar el n&uacute;mero de visitantes y analizar estad&iacute;sticamente la utilizaci&oacute;n que hacen los usuarios de nuestros servicios.</span></span></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">Consentimiento</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Al navegar y continuar en el sitio Web estar&aacute; consintiendo el uso de las cookies antes enunciadas, por los plazos se&ntilde;alados y en las condiciones contenidas en la presente Pol&iacute;tica de Cookies.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Puedes obtener m&aacute;s informaci&oacute;n sobre nosotros a trav&eacute;s de nuestro&nbsp;</span><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"http://wifibytes-front.wearecactus.com/#/avisolegal\"><span style=\"box-sizing: border-box; margin: 0px auto; color: #3d3d3c;\">aviso legal,</span></a><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;nuestra pol&iacute;tica de privacidad, o mediante correo electr&oacute;nico dirigido a la direcci&oacute;n:&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\"><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"mailto:info@wifibytes.com\">info@wifibytes.com</a></span></p>",
            "image": null,
            "lang": "es"
        },
        {
            "key": "aviso legal",
            "content": "<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">DATOS GENERALES</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">De acuerdo con el art&iacute;culo 10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Informaci&oacute;n y de Comercio Electr&oacute;nico ponemos a su disposici&oacute;n los siguientes datos:</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Wifibytes SL. est&aacute; domiciliada en la calle C/Batalla de Lepanto n&ordm;5, Bajo (Bocairent), con CIF B98137078.Inscrita en el Registro Mercantil de Valencia, Vol .9030 , Folio 34, Hoja V-.133779, Inscripci&oacute;n 4&ordf;.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">En la web&nbsp;</span><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"http://wifibytes-front.wearecactus.com/#/\"><span style=\"box-sizing: border-box; margin: 0px auto; color: #3d3d3c;\">http://wifibytes-front.wearecactus.com/#/</span></a><span style=\"box-sizing: border-box; margin: 0px auto;\">hay una serie de contenidos de car&aacute;cter informativo sobre&nbsp;</span><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"http://wifibytes-front.wearecactus.com/#/nosotros\"><span style=\"box-sizing: border-box; margin: 0px auto; color: #3d3d3c;\">http://wifibytes-front.wearecactus.com/#/nosotros</span></a></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Su principal objetivo</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;es facilitar a los clientes y al p&uacute;blico en general, la informaci&oacute;n relativa a la empresa, a los productos y servicios que se ofrecen&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">POL&Iacute;TICA DE PRIVACIDAD</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">En cumplimiento de lo dispuesto en&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">la Ley Org&aacute;nica 15/1999, de 13 de Diciembre, de Protecci&oacute;n de Datos de Car&aacute;cter Personal (LOPD)&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">se informa al usuario que todos los datos que nos proporcione ser&aacute;n incorporados a un fichero, creado y mantenido bajo la responsabilidad de Wifibytes SL</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Siempre</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;se va a respetar la confidencialidad de sus datos personales&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">que s&oacute;lo ser&aacute;n utilizados con la finalidad de gestionar los servicios ofrecidos, atender a las solicitudes que nos plantee, realizar tareas administrativas, as&iacute; como remitir informaci&oacute;n t&eacute;cnica, comercial o publicitaria por v&iacute;a ordinaria o electr&oacute;nica.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Para ejercer sus&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">derechos de oposici&oacute;n, rectificaci&oacute;n o cancelaci&oacute;n&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">deber&aacute; dirigirse a la sede de la empresa C/Batalla de Lepanto, n&ordm;5 Bajo (Bocairent), escribirnos al siguiente correo info@wifibytes.com o ll&aacute;manos al 960 500 606</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">CONDICIONES DE USO</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Las condiciones de acceso y uso del presente sitio web se rigen por la legalidad vigente y por el principio de buena fe</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;comprometi&eacute;ndose el usuario a realizar un buen uso de la web. No se permiten conductas que vayan contra la ley, los derechos o intereses de terceros.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Ser usuario de la web de&nbsp;</span><a style=\"box-sizing: border-box; margin: 0px auto; color: #337ab7; background-color: transparent; text-decoration-line: none;\" href=\"http://web.wifibytes.com/front/#/\"><span style=\"box-sizing: border-box; margin: 0px auto; color: #3d3d3c;\">http://wifibytes.wearecactus.com/front/#/</span></a><span style=\"box-sizing: border-box; margin: 0px auto;\">implica que reconoce haber le&iacute;do y aceptado las presentes condiciones y lo que las extienda la normativa legal aplicable en esta materia. Si por el motivo que fuere no est&aacute; de acuerdo con estas condiciones no contin&uacute;e usando esta web.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Cualquier tipo de notificaci&oacute;n y/o reclamaci&oacute;n solamente ser&aacute; v&aacute;lida por notificaci&oacute;n escrita y/o correo certificado.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">RESPONSABILIDADES</span></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Wifibytes SL no se hace responsable de la informaci&oacute;n y contenidos almacenados en foros, redes sociales o cualesquier otro medio</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;que permita a terceros publicar contenidos de forma independiente en la p&aacute;gina web del prestador.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Sin embargo, teniendo en cuenta los art. 11 y 16 de la LSSI-CE,Wifibytes SL&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">se compromete a la retirada o en su caso bloqueo de aquellos contenidos que pudieran afectar o contravenir la legislaci&oacute;n nacional o internacional</span><span style=\"box-sizing: border-box; margin: 0px auto;\">, derechos de terceros o la moral y el orden p&uacute;blico.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Tampoco la empresa se responsabilizar&aacute; de los da&ntilde;os y perjuicios que se produzcan por fallos o malas configuraciones del software instalado en el ordenador del internauta.</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;Se excluye toda responsabilidad por alguna incidencia t&eacute;cnica o fallo que se produzca cuando el usuario se conecte a internet. Igualmente no se garantiza la inexistencia de interrupciones o errores en el acceso al sitio web.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">As&iacute; mismo Wifibytes SL&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto;\">se reserva el derecho a actualizar, modificar o eliminar la informaci&oacute;n contenida en su p&aacute;gina web</span><span style=\"box-sizing: border-box; margin: 0px auto;\">, as&iacute; como la configuraci&oacute;n o presentaci&oacute;n del mismo, en cualquier momento sin asumir alguna responsabilidad por ello.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Le comunicamos que cualquier precio que pueda ver en nuestra web ser&aacute; solamente orientativo. Si el usuario desea saber con exactitud el precio o si el producto en el momento actual cuenta con alguna oferta de la cual se puede beneficiar ha de acudir a alguna de las tiendas f&iacute;sicas con las que cuenta Wifibytes SL.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">PROPIEDAD INTELECTUAL E INDUSTRIAL</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Wifibytes SL.</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;es titular de todos los derechos sobre el software de la publicaci&oacute;n digital as&iacute; como de los derechos de propiedad industrial e intelectual referidos a los contenidos que se incluyan</span><span style=\"box-sizing: border-box; margin: 0px auto;\">, a excepci&oacute;n de los derechos sobre productos y servicios de car&aacute;cter p&uacute;blico que no son propiedad de esta empresa.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Ning&uacute;n material publicado en esta web podr&aacute; ser reproducido, copiado o publicado</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;sin el consentimiento por escrito de Wifibytes SL.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Toda la informaci&oacute;n que se reciba en la web, como comentarios, sugerencias o ideas, se considerar&aacute; cedida</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;a Wifibytes SL.de manera gratuita. No debe enviarse informaci&oacute;n que NO pueda ser tratada de este modo.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Todos los productos y servicios de estas p&aacute;ginas que NO son propiedad de Wifibytes SL. son marcas registradas de sus respectivos propietarios y son reconocidas como tales por nuestra empresa. Solamente aparecen en la web de Wifibytes SL. a efectos de promoci&oacute;n y de recopilaci&oacute;n de informaci&oacute;n. Estos propietarios pueden solicitar la modificaci&oacute;n o eliminaci&oacute;n de la informaci&oacute;n que les pertenece.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">LEY APLICABLE Y JURISDICCI&Oacute;N</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\" dir=\"ltr\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Las presentes condiciones generales se rigen por la legislaci&oacute;n espa&ntilde;ola.</span><span style=\"box-sizing: border-box; margin: 0px auto;\">&nbsp;Para cualquier litigio que pudiera surgir relacionado con el sitio web o la actividad que en &eacute;l se desarrolla ser&aacute;n competentes Juzgados de Ontinyent, renunciando expresamente el usuario a cualquier otro fuero que pudiera corresponderle.</span></p>",
            "image": null,
            "lang": "es"
        }
    ]
}
let family = [{
    "codfamilia": "0001",
    "slug": "smartphone",
    "nombre": "smartphone",
    "color": {
        "id": 1,
        "titulo": "Blue",
        "hexadecimal": "#2888ea"
    },
    "icono": "http://127.0.0.1:8000/media/familia/icon-smartphone.png",
    "pretitulo": "Lo ultimo en",
    "titulo": "Smartphone",
    "precio_cabecera": 174.0,
    "imagen_cabecera": "http://127.0.0.1:8000/media/familia/bq_aquaris_x_1.png",
    "thumbnail": null,
    "texto_cabecera": "<p>Los mejores terminakles del mercado desde</p>",
    "subtexto_cabecera": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">No olvides que todos nuestros terminales son libres y que no tienes permanencia al contratar nuestras tarifas.</span></p>"
},
{
    "codfamilia": "0002",
    "slug": "tablet",
    "nombre": "tablet",
    "color": {
        "id": 3,
        "titulo": "Verde",
        "hexadecimal": "#00b02e"
    },
    "icono": "http://127.0.0.1:8000/media/familia/tablet.png",
    "pretitulo": "Lo ultimo en",
    "titulo": "Tablets",
    "precio_cabecera": 215.0,
    "imagen_cabecera": "http://127.0.0.1:8000/media/familia/ipadmini_3.png",
    "thumbnail": null,
    "texto_cabecera": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">Los mejores terminales del mercado desde</span></p>",
    "subtexto_cabecera": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">No olvides que todos nuestros terminales son libres y que no tienes permanencia al contratar nuestras tarifas.</span></p>"
}]
let filters = {
    "pantalla": [
        {
            "id": 1,
            "num_pantalla": 5.0
        },
        {
            "id": 2,
            "num_pantalla": 5.2
        },
        {
            "id": 3,
            "num_pantalla": 5.5
        }
    ],
    "procesador": [
        {
            "id": 1,
            "num_procesador": "1,3"
        },
        {
            "id": 2,
            "num_procesador": "2,2"
        },
        {
            "id": 3,
            "num_procesador": "2.1"
        }
    ],
    "ram": [
        {
            "id": 1,
            "num_ram": "1"
        },
        {
            "id": 2,
            "num_ram": "3"
        }
    ],
    "marca": [
        {
            "id": 1,
            "Marca": "Wiko"
        },
        {
            "id": 2,
            "Marca": "BQ"
        },
        {
            "id": 3,
            "Marca": "Xiaomi"
        },
        {
            "id": 4,
            "Marca": "Apple"
        }
    ],
    "camara": [
        {
            "id": 1,
            "num_camara": 5.0
        },
        {
            "id": 2,
            "num_camara": 16.0
        },
        {
            "id": 3,
            "num_camara": 13.0
        }
    ]
}
let articles = [
    {
        "referencia": "3d08e939-0ff1-48fe-b939-7768718ce4a4",
        "templates": {
            "template3": {
                "pretitulo": "",
                "franja_1_texto": "",
                "franja_1_fondo": null,
                "caja_1_titulo": "",
                "caja_1_texto": "",
                "caja_2_titulo": "",
                "caja_2_texto": "",
                "franja_2_texto": "",
                "franja_2_fondo": null,
                "imagen1": null,
                "imagen2": null,
                "imagen3": null,
                "imagen4": null,
                "articulo": null,
                "idioma": null
            },
            "template2": {
                "pretitulo": "",
                "caja_1_titulo": "",
                "caja_1_texto": "",
                "caja_2_titulo": "",
                "caja_2_texto": "",
                "caja_3_titulo": "",
                "caja_3_texto": "",
                "caja_4_titulo": "",
                "caja_4_texto": "",
                "imagen1": null,
                "imagen2": null,
                "imagen3": null,
                "imagen4": null,
                "imagen_fondo_cabecera": null,
                "imagen_fondo_cuerpo": null,
                "articulo": null,
                "idioma": null
            },
            "template1": {
                "id": 1,
                "pretitulo": "Amigable",
                "caja_1_titulo": "Estilo",
                "caja_1_texto": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">Jerry y su acabado metalico ahora se viste con los colores mas frescos.</span></p>",
                "caja_2_titulo": "Tacto",
                "caja_2_texto": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">La pantalla de 5 \"incorpora la tecnologia IPS para una perfecta legibilidad.</span></p>",
                "caja_3_titulo": "Rendimiento",
                "caja_3_texto": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">Gracias a su Quad Core y el sistema Android 6.0 entretenimiento y fluidez estar&aacute;n asegurados.</span></p>",
                "caja_4_titulo": "Caracteristicas del Wiko Jerry:",
                "caja_4_texto": "<ul style=\"box-sizing: border-box; margin: 0px auto 10px; color: #3d3d3c; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px;\">\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Pantalla de 5&Prime;&nbsp;</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Procesador quadcore a 1,3GHz.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">1GB de memoria RAM.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">8GB de almacenamiento interno ampliables por tarjeta microSD.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Bater&iacute;a de 2.000mAh.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">C&aacute;mara trasera de 5MP.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">C&aacute;mara frontal de 2MP</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Android 6.0.</li>\r\n</ul>",
                "imagen1": "media/Templates/wiko_jerry_2_2.png",
                "imagen2": "media/Templates/wiko_jerry_2_3.png",
                "imagen3": "media/Templates/wiko_jerry_2_4.png",
                "imagen_fondo_cabecera": "media/Templates/cabecera.jpg",
                "imagen_fondo_cuerpo": "media/Templates/cuerpo.jpg",
                "articulo": "3d08e939-0ff1-48fe-b939-7768718ce4a4",
                "idioma": 1
            }
        },
        "descripcion": "Wiko Jerry",
        "descripcion_breve": "Wiko Jerry",
        "descripcion_larga": "<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Caracteristicas del&nbsp;<span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">Wiko Jerry:</span></p>\r\n<ul style=\"box-sizing: border-box; margin: 0px auto 10px; color: #3d3d3c; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px;\">\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Pantalla de 5&Prime;&nbsp;</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Procesador quadcore a 1,3GHz.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">1GB de memoria RAM.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">8GB de almacenamiento interno ampliables por tarjeta microSD.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Bater&iacute;a de 2.000mAh.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">C&aacute;mara trasera de 5MP.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">C&aacute;mara frontal de 2MP</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Android 6.0.</li>\r\n</ul>",
        "imagen": "http://127.0.0.1:8000/media/pagina_tarifas/wiko_jerry_2_wUOR5jj.png",
        "thumbnail": "http://127.0.0.1:8000/media/pagina_tarifas/cabecera.jpg",
        "slug": "wiko-jerry",
        "descripcion_va": "",
        "pvp": 125.0,
        "stockfis": 20.0,
        "descripcion_breve_va": "",
        "descripcion_larga_va": "",
        "template": 1,
        "activo": true,
        "visible": true,
        "destacado": false,
        "secompra": true,
        "stockmax": 0.0,
        "codimpuesto": "IVA21%",
        "observaciones": "",
        "codbarras": "",
        "nostock": false,
        "controlstock": false,
        "tipocodbarras": null,
        "sevende": true,
        "venta_online": true,
        "stockmin": 0.0,
        "created_at": 1542186050,
        "updated_at": 1542189496,
        "codfamilia": "0001",
        "marca": 1,
        "pantalla": 1,
        "procesador": 1,
        "ram": 1,
        "camara": 1
    },
    {
        "referencia": "5668cbbd-8a8e-40ef-a1d5-fa149790973e",
        "templates": {
            "template3": {
                "pretitulo": "",
                "franja_1_texto": "",
                "franja_1_fondo": null,
                "caja_1_titulo": "",
                "caja_1_texto": "",
                "caja_2_titulo": "",
                "caja_2_texto": "",
                "franja_2_texto": "",
                "franja_2_fondo": null,
                "imagen1": null,
                "imagen2": null,
                "imagen3": null,
                "imagen4": null,
                "articulo": null,
                "idioma": null
            },
            "template2": {
                "pretitulo": "",
                "caja_1_titulo": "",
                "caja_1_texto": "",
                "caja_2_titulo": "",
                "caja_2_texto": "",
                "caja_3_titulo": "",
                "caja_3_texto": "",
                "caja_4_titulo": "",
                "caja_4_texto": "",
                "imagen1": null,
                "imagen2": null,
                "imagen3": null,
                "imagen4": null,
                "imagen_fondo_cabecera": null,
                "imagen_fondo_cuerpo": null,
                "articulo": null,
                "idioma": null
            },
            "template1": {
                "id": 2,
                "pretitulo": "Excelencia",
                "caja_1_titulo": "Diseño superior.",
                "caja_1_texto": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">Una pantalla que abraza sutilmente el marco met&aacute;lico, con l&iacute;neas limpias y bordes ovalados gracias al cristal 2,5D. Bordes reducidos y una parte trasera de cristal 3D en la que hemos seguido un cuidado proceso de pulido y esmaltado de alt&iacute;sima precisi&oacute;n consiguiendo un tacto tan suave que no podr&aacute;s dejar de tocarlo</span></p>",
                "caja_2_titulo": "Lluvia de colores.",
                "caja_2_texto": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">La tecnolog&iacute;a Quantum Color + nunca hab&iacute;a brillado tanto. Disfruta de m&aacute;s de 16,5 millones de colores en una pantalla de 5,2&rdquo; con resoluci&oacute;n FHD y un brillo de hasta 650 nits. Su pantalla LTPS es a&uacute;n m&aacute;s fina y eficiente y gracias a la tecnolog&iacute;a in-cell, que combina el LCD con la capa t&aacute;ctil, ofrece un &aacute;ngulo de visi&oacute;n mayor. El resultado: im&aacute;genes espectaculares con el m&aacute;ximo realismo.</span></p>",
                "caja_3_titulo": "Mejor autonomía..",
                "caja_3_texto": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">BQ es sin&oacute;nimo de autonom&iacute;a y con Aquaris X Pro lo es m&aacute;s que nunca. Su bater&iacute;a de 3.100 mAh, combinada con componentes de bajo consumo, ofrece hasta un 20% m&aacute;s de autonom&iacute;a que modelos anteriores. Podr&aacute;s navegar durante m&aacute;s de 10 horas o ver tu serie favorita durante m&aacute;s de 11 horas seguidas . Y gracias a la funci&oacute;n Quick Charge 3.0, carga hasta un 50% de bater&iacute;a en s&oacute;lo 30 minutos.</span></p>",
                "caja_4_titulo": "Estas son las características del BQ Aquaris X:",
                "caja_4_texto": "<ul style=\"box-sizing: border-box; margin: 0px auto 10px; color: #3d3d3c; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px;\">\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Pantalla de 5,2 pulgadas IPS Quantum Color +. Resoluci&oacute;n FullHD y 650 nits de brillo.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Procesador Snapdragon 626, octa core a 2,2GHz.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">GPU Adreno 506.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">3GB de memoria RAM.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">32GB de almacenamiento interno (24 utilizables para el usuario).</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Posibilidad de incluir una segunda SIM o una tarjeta microSD de hasta 256GB.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">C&aacute;mara trasera de 16MP (Sony IMX 298) f/2.0, doble flash LED.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">C&aacute;mara frontal de 8MP con f/2.0 y flash LED.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Bater&iacute;a de 3 100 mAh con Quickcharge 3.0.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Lector de huellas, radio FM, NFC, USB tipo C 2.0 OTG, led de notificaciones.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Protecci&oacute;n IP 52 frente a polvo y salpicaduras.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Medidas:&nbsp;146.5 x 72.7 x 7.9 mm.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Peso: 153 gramos.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Android 7.1.1 Nougat.</li>\r\n</ul>",
                "imagen1": "media/Templates/bq_aquaris_x.png",
                "imagen2": "media/Templates/bq_aquaris_x_2.png",
                "imagen3": "media/Templates/bq_aquaris_x_3.png",
                "imagen_fondo_cabecera": "media/Templates/cabecera_ZKApH7i.jpg",
                "imagen_fondo_cuerpo": "media/Templates/cuerpo_QocVv2f.jpg",
                "articulo": "5668cbbd-8a8e-40ef-a1d5-fa149790973e",
                "idioma": 1
            }
        },
        "descripcion": "BQ Aquaris X",
        "descripcion_breve": "BQ Aquaris X",
        "descripcion_larga": "<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\"><span style=\"box-sizing: border-box; margin: 0px auto;\">Estas son las caracter&iacute;sticas del&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px auto; font-weight: bold;\">BQ Aquaris X</span><span style=\"box-sizing: border-box; margin: 0px auto;\">:</span></p>\r\n<ul style=\"box-sizing: border-box; margin: 0px auto 10px; color: #3d3d3c; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px;\">\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Pantalla de 5,2 pulgadas IPS Quantum Color +. Resoluci&oacute;n FullHD y 650 nits de brillo.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Procesador Snapdragon 626, octa core a 2,2GHz.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">GPU Adreno 506.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">3GB de memoria RAM.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">32GB de almacenamiento interno (24 utilizables para el usuario).</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Posibilidad de incluir una segunda SIM o una tarjeta microSD de hasta 256GB.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">C&aacute;mara trasera de 16MP (Sony IMX 298) f/2.0, doble flash LED.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">C&aacute;mara frontal de 8MP con f/2.0 y flash LED.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Bater&iacute;a de 3 100 mAh con Quickcharge 3.0.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Lector de huellas, radio FM, NFC, USB tipo C 2.0 OTG, led de notificaciones.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Protecci&oacute;n IP 52 frente a polvo y salpicaduras.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Medidas:&nbsp;146.5 x 72.7 x 7.9 mm.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Peso: 153 gramos.</li>\r\n<li style=\"box-sizing: border-box; margin: 0px auto; font-family: 'Open Sans'; font-size: 16px; line-height: 26px;\">Android 7.1.1 Nougat.</li>\r\n</ul>",
        "imagen": "http://127.0.0.1:8000/media/pagina_tarifas/bq_aquaris_x_1.png",
        "thumbnail": "http://127.0.0.1:8000/media/pagina_tarifas/cabecera_dMc9ObS.jpg",
        "slug": "bq-aquaris-x",
        "descripcion_va": "",
        "pvp": 300.0,
        "stockfis": 15.0,
        "descripcion_breve_va": "",
        "descripcion_larga_va": "",
        "template": 1,
        "activo": true,
        "visible": true,
        "destacado": false,
        "secompra": true,
        "stockmax": 0.0,
        "codimpuesto": "IVA21%",
        "observaciones": "",
        "codbarras": "",
        "nostock": false,
        "controlstock": false,
        "tipocodbarras": null,
        "sevende": true,
        "venta_online": true,
        "stockmin": 0.0,
        "created_at": 1542187095,
        "updated_at": 1542189483,
        "codfamilia": "0001",
        "marca": 2,
        "pantalla": 2,
        "procesador": 2,
        "ram": 2,
        "camara": 2
    }]
export {dataJumbotron,dataContact,dataTariffs,home,datos_empresa,articles,family,filters};