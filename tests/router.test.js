import {Router} from '../src/router';
window.location.href = '#home';

test('Test function getFragment', () => {
    expect(Router.getFragment()).toBe("home");
});

test('Test function clearSlashes', () => {
    expect(Router.clearSlashes("home")).toBe("home");
});

test('Test function add, remove and flush', () => {
    expect(Router.routes.length).toBe(0);
    Router.add(/contact/, function() {});
    expect(Router.routes.length).toBe(1);
    Router.add(/catalogue/, function() {});
    expect(Router.routes.length).toBe(2);
    Router.remove(/contact/);
    expect(Router.routes.length).toBe(1);
    Router.flush();
    expect(Router.routes.length).toBe(0);
});

