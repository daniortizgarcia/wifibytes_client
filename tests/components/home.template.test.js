import HomeComponent from '../../src/component/home.component';
import { dataJumbotron,dataTariffs,home } from '../jsontest';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<div id="bodyContent" class="bodyContent"></div>';
  
});

test('Test function render', () => {
    let dataHome = []
    dataHome.dataJumbotron = dataJumbotron;
    dataHome.tariffs = dataTariffs;
    dataHome.home = home;
    new HomeComponent(dataHome,'#bodyContent');
    expect($('#bodyContent .jumbotron').length).toBeGreaterThan(0);
    expect($('#bodyContent .containerTariffs').length).toBeGreaterThan(0);
    expect($('#bodyContent .containerContent').length).toBeGreaterThan(0);
    expect($('#bodyContent').html().match(/undefined/i)).toBe(null);
});

test('Test component home', () => {
    let dataHome = []
    dataHome.dataJumbotron = dataJumbotron;
    dataHome.tariffs = dataTariffs;
    dataHome.home = home;
    let homeCom = new HomeComponent(dataHome,'#bodyContent');
    expect(homeCom.constructor.name).toBe('HomeComponent');
});

test('Component must fail due to target html tag to render in doesnt exist', () => { 
    let dataHome = []
    dataHome.dataJumbotron = dataJumbotron;
    dataHome.tariffs = dataTariffs;
    dataHome.home = home;
    expect(function(){new HomeComponent(dataHome,"#bodyConten")}).toThrowError(/Error/i);    
});
