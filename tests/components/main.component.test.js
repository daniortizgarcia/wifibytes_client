import MainComponent from '../../src/component/main.component';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<div id="bodyContent" class="bodyContent"></div>';
  
});

test('Test function render', () => {
    let data = [{content:"Texto de prueba"}];
    new MainComponent(data,'#bodyContent',"cookies");
    expect($('#bodyContent #containerMain').length).toBeGreaterThan(0);
    expect($('#bodyContent').html().match(/undefined/i)).toBe(null);
});

test('Test component main', () => {
    let data = [{content:"Texto de prueba"}];
    let mainCom = new MainComponent(data,'#bodyContent');
    expect(mainCom.constructor.name).toBe('MainComponent');
});

test('Component must fail due to target html tag to render in doesnt exist', () => { 
    let data = [{content:"Texto de prueba"}];
    expect(function(){new MainComponent(data,"#bodyConten")}).toThrowError(/Error/i);    
});
