import NavComponent from '../../src/component/nav.component';
import {datos_empresa} from '../jsontest';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<div id="headerContent"></div>';
  
});

test('Test function render', () => {
    new NavComponent(datos_empresa,'#headerContent');
    expect($('#headerContent .nav').length).toBeGreaterThan(0);
    expect($('#headerContent').html().match(/undefined/i)).toBe(null);
});

test('Test component Header', () => {
    let HeaderComp = new NavComponent(datos_empresa,'#headerContent');
    expect(HeaderComp.constructor.name).toBe('HeaderComponent');
});

test('Component must fail due to target html tag to render in doesnt exist', () => { 
    expect(function(){new NavComponent(datos_empresa,"#headerConten")}).toThrowError(/Error/i);    
});
