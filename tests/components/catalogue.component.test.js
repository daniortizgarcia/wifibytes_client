import CatalogueComponent from '../../src/component/catalogue.component';
import {articles,family,filters} from '../jsontest';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML =
    '<div id="bodyContent" class="bodyContent"></div>';
});

test('Test component Catalogue', () => {
    let contactCom = new CatalogueComponent(articles,family,filters,'#bodyContent');
    expect(contactCom.constructor.name).toBe('CatalogueComponent');
});

test('Component must fail due to target html tag to render in doesnt exist', () => { 
    expect(function(){new CatalogueComponent(articles,family,filters,"#bodyConten")}).toThrowError(/Error/i);    
});

test('Test function Catalogue', () => {
    new CatalogueComponent(articles,family,filters,'#bodyContent');
    expect($('#bodyContent .header__article').length).toBeGreaterThan(0);
    expect($('#bodyContent').html().match(/undefined/i)).toBe(null);
});