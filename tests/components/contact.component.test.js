import ContactComponent from '../../src/component/contact.component';
import {dataContact} from '../jsontest';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML =
    '<div id="bodyContent" class="bodyContent"></div>';
    window.google = {
        maps: {
            Map:class {},
            Marker: class {}
        }
    };
});

test('Test component Contact', () => {
    let contactCom = new ContactComponent(dataContact,'#bodyContent');
    expect(contactCom.constructor.name).toBe('ContactComponent');
});

test('Component must fail due to target html tag to render in doesnt exist', () => { 
    expect(function(){new ContactComponent(dataContact,"#bodyConten")}).toThrowError(/Error/i);    
});

test('Test function render', () => {
    new ContactComponent(dataContact,'#bodyContent');
    expect($('#bodyContent #contactContent').length).toBeGreaterThan(0);
    expect($('#bodyContent').html().match(/undefined/i)).toBe(null);
});

test('Test function validateForm', () => {
    let contactCom = new ContactComponent(dataContact,'#bodyContent');
    expect(contactCom.validateForm().data).toBe("The name must be between 3 and 25 characters");  
    document.getElementById('contactName').value = "Dani";
    expect(contactCom.validateForm().data).toBe("The phone isn\'t valid");
    document.getElementById('contactPhone').value = 666666666;
    expect(contactCom.validateForm().data).toBe("The email isn\'t valid");   
    document.getElementById('contactEmail').value = "danipuerta54@gmail.com"; 
    expect(contactCom.validateForm().data).toBe("The message must be between 20 and 200 characters");  
    document.getElementById('contactMessage').value = "Hola este es un mensaje de prueba";
    expect(contactCom.validateForm().valid).toBe(true);
});

test('Test function initMap', () => {
    window.google = {
        maps: {
            Map:class {},
            Marker: class {}
        }
    };
    let contactCom = new ContactComponent(dataContact,'#bodyContent');
    contactCom.initMap(0,0,"Daniel")
})