import TariffsComponent from '../../src/component/tariffs.component';
import { dataTariffs } from '../jsontest';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<div id="bodyContent" class="bodyContent"></div>';
  
});

test('Test function templateTariffs ', () => {
    new TariffsComponent(dataTariffs,'#bodyContent');
    expect($('#bodyContent .containerTariffs').length).toBeGreaterThan(0);
    expect($('#bodyContent').html().match(/undefined/i)).toBe(null);
});

test('Test component main', () => {
    let tariffsCom = new TariffsComponent(dataTariffs,'#bodyContent');
    expect(tariffsCom.constructor.name).toBe('TariffsComponent');
});

test('Component must fail due to target html tag to render in doesnt exist', () => { 
    expect(function(){new TariffsComponent(dataTariffs,'#bodyConten')}).toThrowError(/Error/i);    
});