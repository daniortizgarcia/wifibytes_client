import FooterComponent from '../../src/component/footer.component';
import {datos_empresa} from '../jsontest';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<div id="footerContent"></div>';
});

test('Test function render', () => {
    new FooterComponent(datos_empresa,'#footerContent');
    expect($('#footerContent #containerFooter').length).toBeGreaterThan(0);
    expect($('#footerContent').html().match(/undefined/i)).toBe(null);
});

test('Test component Footer', () => {
    let footerCom = new FooterComponent(datos_empresa,'#footerContent');
    expect(footerCom.constructor.name).toBe('FooterComponent');
});

test('Component must fail due to target html tag to render in doesnt exist', () => { 
    expect(function(){new FooterComponent(datos_empresa,"#footerConten")}).toThrowError(/Error/i);    
});