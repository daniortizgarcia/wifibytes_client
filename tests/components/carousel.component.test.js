import CarouselComponent from '../../src/component/carousel.component';
import {dataJumbotron} from '../jsontest';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =
  '<div id="bodyContent" class="bodyContent"></div>';
  
});

test('Test function jumbotron', () => {
    document.getElementById('bodyContent').innerHTML = CarouselComponent.render(dataJumbotron);
    expect($('#bodyContent #jumbotron').length).toBeGreaterThan(0);
    expect($('#bodyContent').html().match(/undefined/i)).toBe(null);
});

test('Test function startSlider and modeSlide', () => {
    document.getElementById('bodyContent').innerHTML = CarouselComponent.render(dataJumbotron);
    CarouselComponent.startSlider();
    let slide = Array.from(document.getElementsByClassName("slide"));
    expect(slide[0].style.display).toBe("block");
    $('#slideNext').click();
    expect(slide[1].style.display).toBe("block");
    $('#slidePrev').click();
    expect(slide[0].style.display).toBe("block");
    $('.dot')[2].click();
    expect(slide[2].style.display).toBe("block");
});
