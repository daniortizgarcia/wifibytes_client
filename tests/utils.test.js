import {request,getCookies,setCookies,delCookies} from '../src/utils';

const mockXHR = {
    open: jest.fn(),
    send: jest.fn(),
    readyStatus: 200,
    responseText: JSON.stringify(
        [{
            "pk": 1,
            "titulo": "Texto Principal",
            "subtitulo": "Texto principal del Home",
            "caja_izquierda_titulo": "¿Por que Wicofi?",
            "caja_izquierda_texto": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">&iquest;Quieres darte de baja? &iquest;Cambiar &nbsp;de tarifa? &iquest; Activar el roaming? &iquest;Desactivar el buzon de voz? Sin problema. Entra en tu cuenta y hazlo con un solo click.Sin preguntas o llamadas a telefonos asistidos.</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">Simple y facil. Todo el soporte y atenci&oacute;n v&iacute;a internet con un sistema de tickets.</span></p>",
            "caja_derecha_titulo": "¿Por qué Wicofi?",
            "caja_derecha_texto": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">&iquest;Quieres darte de baja? &iquest;Cambiar &nbsp;de tarifa? &iquest; Activar el roaming? &iquest;Desactivar el buzon de voz? Sin problema. Entra en tu cuenta y hazlo con un solo click.Sin preguntas o llamadas a telefonos</span></p>",
            "activo": true,
            "idioma": 1,
            "lang": "es"
        },
        {
            "pk": 2,
            "titulo": "Texto Secundario",
            "subtitulo": "Texto principal del Home",
            "caja_izquierda_titulo": "¿Por que Wicofi?",
            "caja_izquierda_texto": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">&iquest;Quieres darte de baja? &iquest;Cambiar &nbsp;de tarifa? &iquest; Activar el roaming? &iquest;Desactivar el buzon de voz? Sin problema. Entra en tu cuenta y hazlo con un solo click.Sin preguntas o llamadas a telefonos asistidos.</span></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">Simple y facil. Todo el soporte y atenci&oacute;n v&iacute;a internet con un sistema de tickets.</span></p>",
            "caja_derecha_titulo": "¿Por qué Wicofi?",
            "caja_derecha_texto": "<p><span style=\"color: #3d3d3c; font-family: 'Open Sans'; font-size: 16px;\">&iquest;Quieres darte de baja? &iquest;Cambiar &nbsp;de tarifa? &iquest; Activar el roaming? &iquest;Desactivar el buzon de voz? Sin problema. Entra en tu cuenta y hazlo con un solo click.Sin preguntas o llamadas a telefonos</span></p>",
            "activo": true,
            "idioma": 1,
            "lang": "es"
        }]
    )
};
const mockXHRError = {
    open: jest.fn(),
    send: jest.fn(),
    readyStatus: 404,
    responseText: "Error 404: Url not found"
};
test('Test function request True', () => {
    window.XMLHttpRequest = jest.fn(() => mockXHR);
    let data = [];
    const reqPromise = request("","GET",data);
    mockXHR.onload();
        reqPromise.then((response) => {
            expect(response.length).toBe(2);
            expect(response[0].titulo).toBe("Texto Principal");
            expect(response[1].titulo).toBe("Texto Secundario");
            done();
        });
});

test('Test function request Error', () => {
    window.XMLHttpRequest = jest.fn(() => mockXHRError);
    let data =  [];
    const reqPromise = request("","GET",data);
    mockXHRError.onload();
        reqPromise.then((response) => {
            expect(response.length).toBe(0);
            expect(response).toBe('Error 404: Url not found');
            done();
        });
});

test('Test setCookies,getCookies,delCookies',() => {
    expect(getCookies("lang")).toBe(false);
    setCookies("lang","es",300);
    expect(getCookies("lang")).toBe("es");
    delCookies("lang");
    expect(getCookies("lang")).toBe(false);
})
