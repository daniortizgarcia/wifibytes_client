#INTRODUCTION

This silly project is only an excuse to explain the basic scaffolding of a SPA web application.
The project is programmed in raw ES6 (ES2015) and transpiled, packaged, uglified and served using as a supporting tools; webpack, babel, webpack-dev-server, jsdoc ...
Could be used as a base to develop any other javascript project during the course

#Key points 
* Unit battery testing with jest
* Hot live reloading with webpack hmr
* PAckaged with webpack
* Transpiled with babel
* Documented with jsdoc

#Webpack

NICE TO READ https://www.valentinog.com/blog/webpack-tutorial/

#Ajax and fetch API 
NICE TO READ: https://dev.to/bjhaid_93/beginners-guide-to-fetching-data-with-ajax-fetch-api--asyncawait-3m1l

#TIPS
To solve some jest testing problems I've been forced to install

npm install babel-core@7.0.0-bridge.0 --save-dev

#ESPAÑOL

# Mejoras
- La aplicación utiliza solo un componente para los modulos de cookies,legal y nosotros.
- El sistema de caching utilizando localStorage, para cuando inicies de nuevo el ordenador todavía tengas los datos guardados.
- La función del carrusel la puedes reutilizar fácilmente. 

# Detalles
- El carrusel recoge tanto imágenes como vídeos del servidor, cogiendo su extensión i comparándola para ver si es una imagen o un vídeo. Si no te se muestra la imagen puede ser que la extensión no este incluida. También puedes pausar o iniciar un cambiador automatico de diapositiva.
- El aviso legal tiene una expresión regular que debe encontrar "legal" (Con mayus o minus).
- Las cookies tiene una expresión regular que debe encontrar "cookies" (Con mayus o minus).
- Nosotros tiene una expresión regular que debe encontrar "nosotros" (Con mayus o minus).
- Los iconos utilizado son de fontawesome.com.
- Para pintar Google Maps utilizo la api de google.
- El formulario de contacto tiene una validación.
- Utilización de flexbox en la aplicación.
- Utilizada la metodología BEM.
- Grid layout utilizado en el modulo de catálogo con media querys para hacer responsive los articulos.
- Comentarios para crear el jsdoc y comentarios para que el código sea mas fácil de leer.
- Puedes cambiar de idiomas, español, ingles y valenciano. Cambia los datos del Servidor como del Cliente.
- Si no existe el texto a mostrar del servidor en ese idioma saltara un aviso.
- Creado modulo de catálogo donde se listan los articulos.
- Puedes filtrar en el modulo de catalogo pero familia o por caracteristicas.
- Si al filtrar no hay ninguna coincidencia saldra un mensaje de aviso.

#ENGLISH

# Improvements
- The application uses only one component for the cookie modules, legal and us.
- The caching system using localStorage, for when you restart the computer you still have the data saved.
- You can easily reuse the carousel function. 

# Details
- The carousel collects both images and videos from the server, taking its extension and comparing it to see if it is an image or a video. If you are not show the image may be that the extension is not included. You can also pause or start an automatic slide changer.
- The legal notice has a regular expression that you should find "legal" (with a capital or minus).
- Cookies have a regular expression that must find "cookies" (with a capital or minus).
- Nosotros have a regular expression that must find "Nosotros" (with a capital or minus).
- The icons used are from fontawesome.com.
- To paint Google Maps I use the api of google.
- The contact form has a validation. (If the data are correct gives error as it tries to send the mail but is not implemented this function)
- The language change is not yet implemented even though it is the select one.
- Use of flexbox in the application.
- The BEM methodology was used.
- Grid layout used in the catalog module with media querys to make responsive articles.
- Comments to create the jsdoc and comments to make the code easier to read.
- You can change languages, Spanish, English and Valencian. Change the data of the Server as well as the Client.
- If there is no text to be displayed from the server in that language, a warning will be triggered.
- Created a catalogue module where the articles are listed.
- You can filter in the catalog module by family or characteristics.
- If there is no match when filtering, a warning message will appear.