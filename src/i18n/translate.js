/* English and Valencian Json*/
let languages = {
    "en": {
        "Es":"Sp",
        "In":"En",
        "Inicio":"Home",
        "Tarifas":"Tariffs",
        "Catálogo":"Catalogue", 
        "Contacto":"Contact",
        "Nosotros":"We",
        "aviso legal":"legal",
        "cookies":"cookies",
        "nosotros":"we",
        "gratis":"free",
        "fijos nacionales":"national fixes",
        "establecimiento llamada":"establishment called",
        "subida":"uphill",
        "bajada":"downturn",
        "fibra optica":"optical fiber",
        "Paquete fútbol":"Football package",
        "mes":"month",
        "Contratar":"Contract",
        "Más info":"More info",
        "Marcas":"Brands",
        " Todo":" All",
        "Pulgadas":"Inches",
        "Procesadores":"Processors",
        "Camaras":"Cameras",
        "Ninguna coincidencia":"No coincidence",
        "Los mejores artículos":"The best articles",
        "Enviar mensaje":"Send Message",
        "El nombre debe tener entre 3 y 25 caracteres":"The name must be between 3 and 25 characters",
        "El teléfono no es válido":"The phone isn't valid",
        "El Correo electronico no es válido":"The email isn't valid",
        "El mensaje debe tener entre 20 y 200 caracteres":"The message must be between 20 and 200 characters",
        "Va":"Va",
        "Elemento solicitado sin traducción, pronto estará disponible.":"Element requested without translation, will soon be available."
    },
    "va": {
        "Es":"Es",
        "In":"In",
        "Inicio":"Inici",
        "Tarifas":"Tarifes",
        "Catálogo":"Catàleg", 
        "Contacto":"Contacte",
        "Nosotros":"Nosaltres",
        "aviso legal":"avis legal",
        "cookies":"cookies",
        "nosotros":"nosaltres",
        "gratis":"gratis",
        "fijos nacionales":"fixos nacionals",
        "establecimiento llamada":"establiment crida",
        "subida":"muntada",
        "bajada":"baixada",
        "fibra optica":"fibra òptica",
        "Paquete fútbol":"Paquet futbol",
        "mes":"mes",
        "Contratar":"Contractar",
        "Más info":"Mes info",
        "Marcas":"Marques",
        " Todo":" Tot",
        "Pulgadas":"Polzades",
        "Procesadores":"Procesadors",
        "Camaras":"Camares",
        "Ninguna coincidencia":"Cap coincidència",
        "Los mejores artículos":"Els millors articles",
        "Enviar mensaje":"Enviar missatge",
        "El nombre debe tener entre 3 y 25 caracteres":"El nom ha de tindre entre 3 i 25 caràcters",
        "El teléfono no es válido":"El telèfon no és vàlid",
        "El Correo electronico no es válido":"El Correu electrònic no és vàlid",
        "El mensaje debe tener entre 20 y 200 caracteres":"El missatge ha de tindre entre 20 i 200 caràcters",
        "Va":"Va",
        "Elemento solicitado sin traducción, pronto estará disponible.":"Element solicitat sense traducció, pronte estará dinspoble."
    }
  };
/**
 * Change the language
 */
function changeLang(lang) {
    /* Get all the data-lang of html(data-lang="") and if lang exist in languages get info from JSON if not get info from HTML */
    Array.from(document.querySelectorAll('[data-lang]')).map( element => {
        element.innerHTML = languages.hasOwnProperty(lang) ? languages[lang][element.dataset.lang] : element.dataset.lang; 
    });
}

/* Export the function changeLang */
export {changeLang};
  