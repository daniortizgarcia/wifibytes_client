import Component from "./component";

class CatalogueComponent extends Component {
    /**
     * Class CatalogueComponent
     * @param {Array} catalogue 
     * @param {Array} family 
     * @param {Json} filter 
     * @param {String} container 
     */
    constructor(catalogue,family,filter,container){
        super(container)
        /* Call the function render if container exist */
        document.querySelector(container).innerHTML = this.render(family,filter);

        let familySection = Array.from(document.getElementsByClassName("family__section"));
        familySection.map(element => {
            element.addEventListener('click',()=>{this.headerFamily(element.id,family,catalogue)});
        })

        this.headerFamily(family[0].codfamilia,family,catalogue)
        document.getElementById('selectBranch').addEventListener('change',()=>{this.filterCatalogue(this.catalog)});
        document.getElementById('selectInc').addEventListener('change',()=>{this.filterCatalogue(this.catalog)});
        document.getElementById('selectRam').addEventListener('change',()=>{this.filterCatalogue(this.catalog)});
        document.getElementById('selectProcessor').addEventListener('change',()=>{this.filterCatalogue(this.catalog)});
        document.getElementById('selectCamera').addEventListener('change',()=>{this.filterCatalogue(this.catalog)});
    }

    /**
     * Load Catalogue template
     * @param {Array} dataFamily 
     * @param {Json} filter 
     * @return {String} Return HTML Catalogue
     */
    render(dataFamily,filter){
        /* Create HTML of families*/
        let familyHTML = dataFamily.map( element => {
            return `
                <section id="${element.codfamilia}" class="family__section">
                    <img style="max-width:40px" src="${element.icono}" />
                    <label>${element.titulo}</label>
                </section>
            `;
        })

        /* Create the HTML of Filter*/
        let filterHTML = 
            `
            <section class="filter__section">
                <p data-lang="Marcas"></p>
                <select id="selectBranch" class="filter__select">
                    <option value="*" data-lang=" Todo"></option>
                    ${
                        filter.marca.map(element => {
                            return `
                                <option value="${element.id}">${element.Marca}</option>
                            `;
                        }).join('')
                    }
                </select>
            </section>
            <section class="filter__section">
                <p data-lang="Pulgadas"></p>
                <select id="selectInc" class="filter__select">
                    <option value="*" data-lang=" Todo"></option>
                    ${
                        filter.pantalla.map(element => {
                            return `
                                <option value="${element.id}">${element.num_pantalla}</option>
                            `;
                        }).join('')
                    }
                </select>
            </section>
            <section class="filter__section">
                <p>Ram</p>
                <select id="selectRam" class="filter__select">
                    <option value="*" data-lang=" Todo"></option>
                    ${
                        filter.ram.map(element => {
                            return `
                                <option value="${element.id}">${element.num_ram}</option>
                            `;
                        }).join('')
                    }
                </select>
            </section>
            <section class="filter__section">
                <p data-lang="Procesadores"></p>
                <select id="selectProcessor" class="filter__select">
                    <option value="*" data-lang=" Todo"></option>
                    ${
                        filter.procesador.map(element => {
                            return `
                                <option value="${element.id}">${element.num_procesador}</option>
                            `;
                        }).join('')
                    }
                </select>
            </section>
            <section class="filter__section">
                <p data-lang="Camaras"></p>
                <select id="selectCamera" class="filter__select">
                    <option value="*" data-lang=" Todo"></option>
                    ${
                        filter.camara.map(element => {
                            return `
                                <option value="${element.id}">${element.num_camara}</option>
                            `;
                        }).join('')
                    }
                </select>
            </section>
        `;

        /* Return the Html catalogue*/ 
        return `
            <article class="header__article">
                <h1 style="text-align:center; margin-top:30px;" data-lang="Catálogo"></h1>
                <h3 style="text-align:center; margin-top:10px;" data-lang="Los mejores artículos"></h3>
            </article>
            <article class="family__article">
                ${familyHTML.join('')}
            </article>
            <article class="filter__article">
                ${filterHTML}
            </article>
            <article class="catalogue__article">
            </article>
            <label class='error__catalogue' id='cataError' style="visibility:hidden;" data-lang='Ninguna coincidencia'></label>
        ` ;  
    }

    /**
     * Filter articles of catalogue
     * @param {Array} catalogue 
     */
    filterCatalogue(catalogue){
        document.getElementById('cataError').style.visibility = "hidden";
        let reg = "";
        let branch = document.getElementById('selectBranch').value;
        let inc = document.getElementById('selectInc').value;
        let ram = document.getElementById('selectRam').value;
        let processor = document.getElementById('selectProcessor').value;
        let camera = document.getElementById('selectCamera').value;

        /* If branch filter isn't * filter */
        if(branch != "*"){
            reg = new RegExp(`^${branch}$`);
            catalogue = catalogue.filter(element => String(element.marca).match(reg));
        }

        /* If branch filter isn't * filter */
        if(inc != "*"){
            reg = new RegExp(`^${inc}$`);
            catalogue = catalogue.filter(element => String(element.pantalla).match(reg));
        }

        /* If branch filter isn't * filter */
        if(ram != "*"){
            reg = new RegExp(`^${ram}$`);
            catalogue = catalogue.filter(element => String(element.ram).match(reg));
        }

        /* If branch filter isn't * filter */
        if(processor != "*"){
            reg = new RegExp(`^${processor}$`);
            catalogue = catalogue.filter(element => String(element.procesador).match(reg));
        }

        /* If branch filter isn't * filter */
        if(camera != "*"){
            reg = new RegExp(`^${camera}$`);
            catalogue = catalogue.filter(element => String(element.camara).match(reg));
        }
        /* If catalogue have lenght is 0, print missatge warning, else print the articles */
        if(catalogue.length < 1){
            document.getElementById('cataError').style.visibility = "visible";
            document.querySelector('.catalogue__article').innerHTML = "";
        }else
            document.querySelector('.catalogue__article').innerHTML = this.catalogue(catalogue);
    }

    /**
     * Print HTML of the articles
     * @param {Array} dataCatalogue 
     */
    catalogue(dataCatalogue){
        /* return the html of the articles */
        return dataCatalogue.map( element => {
            return `
                <section class="catalogue__section">
                    <img class="catalogue__img" src="${element.imagen}" />
                    <h3 class="catalogue__h3">${element.descripcion}</h3>
                    <h4 class="catalogue__h4">${element.descripcion_breve}</h4>
                </section>
            `;
        }).join('')
    }

    /**
     * Print header HTML of the familie and filtred
     * @param {String} id 
     * @param {Array} family 
     * @param {Array} catalogue 
     */
    headerFamily(id,family,catalogue){
        /* Create a regular expression */
        let reg = new RegExp(`^${id}$`);
        /* Use a regular expression to get a family needed */
        let fam = family.filter(element => element.codfamilia.match(reg));
        catalogue = catalogue.filter(element => element.codfamilia.match(reg));
        this.catalog = catalogue;
        this.filterCatalogue(catalogue)
        document.querySelector('.header__article').innerHTML = `
            <section style="background-color:${fam[0].color.hexadecimal};" class="headfamily">
                <img class="headfamily__img" src="${fam[0].imagen_cabecera}" style="width:300px;" />
                <section>
                    <h3 class="headfamily__h3">${fam[0].pretitulo}</h3>
                    <h2 class="headfamily__h2">${fam[0].titulo}</h2>
                    <br/>
                    <h4 class="headfamily__h4">${fam[0].texto_cabecera}</h4>
                    <h2 class="headfamily__h2">${fam[0].texto_cabecera}</h2>
                    <h5 class="headfamily__h5">${fam[0].subtexto_cabecera}</h5>
                </section>
            </section>
        `;
    }

}

/* Export the class CatalogueComponent */
export default CatalogueComponent;