import CarouselComponent from "./carousel.component";
import TariffsComponent from "./tariffs.component";
import Component from "./component";

class HomeComponent extends Component{
    /**
     * Class HomeComponent
     * @class
     * @param {Array} data 
     * @param {String} container 
     */
    constructor(data,container){
        super(container)
        /* Call the function render if container exist */
        document.querySelector(container).innerHTML = this.render(data);
    }

    /**
     * Load the Home template
     * @function
     * @param {Array} dataHome
     * @return {string} Return the HTML carousel, tariffs and data home
     */
    render(dataHome){
        /* Declare the var where save the HTML*/
        let contentHTML = "";

        let Tariffs = new TariffsComponent(dataHome.tariffs,"#bodyContent");
        /* Map of dataHome texts if length is less than 1, send warning message */
        if(dataHome.home.length > 0){
            dataHome.home.map( data => {
                contentHTML  += `
                    <section class="sectionBody">
                        <h3 class="sectionBody__h3">${data.caja_izquierda_titulo}</h3>
                        <section class="sectionBody__section">${data.caja_izquierda_texto}</section>
                    </section>
                    <section class="sectionBody">
                        <h3 class="sectionBody__h3">${data.caja_derecha_titulo}</h3>
                        <section class="sectionBody__section">${data.caja_derecha_texto}</section>
                    </section>  
                `;
            })
        }else{
            contentHTML = "<label class='warning' data-lang='Elemento solicitado sin traducción, pronto estará disponible.'></label>"
        }
        
        /* return home HTML */
        return `
            ${ dataHome.dataJumbotron ?  CarouselComponent.render(dataHome.dataJumbotron) : ""}
            ${ dataHome.tariffs ? Tariffs.render(dataHome.tariffs) : ""}
            <article class="containerContent">
                ${contentHTML}
            </article>
        `;
    };

}

/* Export the class HomeComponent */
export default HomeComponent;