import Component from "./component";

class TariffsComponent extends Component{
    /**
     * Class TariffsComponent
     * @class
     * @param {Array} data 
     * @param {String} container 
     */
    constructor(data,container){
        super(container)
        /* Call the function render if container exist */
        document.querySelector(container).innerHTML = this.render(data);
    }

    /**
     * Load the Tariffs template
     * @function
     * @param {Array} dataTariffs 
     * @return {String} Return the HTML tariffs
     */
    render(dataTariffs){
        /* Declare the var where save the HTML*/
        let tariffsHTML = "";
        let subtariffsHTML = "";
    
        /* Map of dataTariffs */
        dataTariffs.map( data =>{
            /* clear the var */
            subtariffsHTML = "";
            data.subtarifas ? true : data.subtarifas = [];
    
            /* forEeach for each subtarifa */
            data.subtarifas.forEach(element => {
                /* switch case */
                switch(element.tipo_tarifa)  {
                    /* MobliePhone */
                    case 1:
                        subtariffsHTML += `
                            <section class="bodySubTariffs__section">
                                <section>
                                    <i class="fas fa-mobile-alt"></i><br />
                                    <label><strong>${element.subtarifa_minutos_gratis} min/<label data-lang="mes"></label></strong> <label data-lang="gratis"></label></label><br />
                                    <label><strong>${element.subtarifa_datos_internet}GB/<label data-lang="mes"></label></strong> 4g</label>
                                </section>
                            </section>
                        `; 
                        break;
                    /* Phone */
                    case 2: 
                        subtariffsHTML += `
                            <section class="bodySubTariffs__section">
                                <section>
                                    <i class="fas fa-phone"></i><br />
                                    <label><strong>${element.subtarifa_cent_minuto}€/min</strong> <label data-lang="fijos nacionales"></label></label><br />
                                    <label><strong>${element.subtarifa_est_llamada}</strong> <label data-lang="establecimiento llamada"></label></label>
                                </section>
                            </section>
                        `;
                        break;
                    /* optical fiber */
                    case 3:
                        subtariffsHTML += `
                            <section class="bodySubTariffs__section">
                                <section>
                                    <i class="fas fa-globe"></i><br />
                                    <label><strong>${element.subtarifa_velocidad_conexion_bajada} <label data-lang="subida"></label></strong> <label data-lang="fibra optica"></label><br />
                                    <label><strong>${element.subtarifa_velocidad_conexion_subida} <label data-lang="bajada"></label></strong> <label data-lang="fibra optica"></label></label>
                                </section>
                            </section>
                        `; 
                        break;
                    /* WIFI */
                    case 4: 
                        subtariffsHTML += `
                            <section class="bodySubTariffs__section">
                                <section>
                                    <i class="fas fa-wifi"></i><br />
                                    <label><strong>${element.subtarifa_velocidad_conexion_bajada} <label data-lang="subida"></label></strong> wifi</label><br />
                                    <label><strong>${element.subtarifa_velocidad_conexion_subida} <label data-lang="bajada"></label></strong> wifi</label>
                                </section>
                            </section>
                        `;
                        break;
                    /* TV */
                    case 5: 
                        subtariffsHTML += `
                            <section class="bodySubTariffs__section">
                                <section>
                                    <i class="fas fa-tv"></i><br />
                                    <label><strong>${element.subtarifa_num_canales} canales</strong> <label data-lang="gratis"></label></label><br />
                                    <label><strong><label data-lang="Paquete fútbol"></strong> <label data-lang="gratis"></label></label>
                                </section>
                            </section>
                        `;
                        break;
                    default:
                        break;
                } 
            })
    
            /* HTML from all the tariff */
            tariffsHTML += `
                <section class="tariffsSection">
                    <section class="headerSection">
                        <img class="headerSection__image" src="${data.logo}" />
                        <h3 class="headerSection__h3">
                            ${data.nombretarifa}
                        </h3>
                    </section>
                    <section class="bodySection">   
                        <label class="bodySection__label">${data.precio} €/<label data-lang="mes"></label></label>
                    </section>
                    <section class="bodySubTariffs">
                        ${subtariffsHTML}
                    </section>
                    <section class="footerSection">
                        <button class="footerSection__button" type="button" data-lang="Contratar"></button>
                        <button class="footerSection__button" type="button" data-lang="Más info"></button>
                    </section>
                </section>
            `;
        })
        
        return `
            <article class="containerTariffs">
               ${tariffsHTML}
            </article>
        `;
    }

}

/* Export the class TariffsComponent */
export default TariffsComponent;