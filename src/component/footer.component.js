import Component from "./component";
import { setCookies } from "../utils";
import {Router} from '../router';


class FooterComponent extends Component{
    /**
     * Class FooterComponent
     * @class
     * @param {Array} dataCompany 
     * @param {String} container 
     */
    constructor(dataCompany,container){
        super(container)
        /* Call the function render if container exist */
        document.querySelector(container).innerHTML = this.render();
        
        /* ForEach for empresa texts */
        dataCompany.textos.forEach( element => {
            /* Check the match */
            if(element.key.match(/legal/gi) && !document.getElementById('legal')){
                /* Create element a and print to footerNavSpan */
                document.getElementById('footerNavSpan').innerHTML += '<a id="legal" class="fnav__a" href="#legal"><label data-lang="aviso legal"></label> | </a>';
            }
            
            if(element.key.match(/cookies/gi) && !document.getElementById('cookies')){
                /* Create element a and print to footerNavSpan */
                document.getElementById('footerNavSpan').innerHTML += '<a id="cookies" class="fnav__a" href="#cookies">cookies | </a>';
            }
            
            if(element.key.match(/nosotros/gi) && !document.getElementById('nosotros')){

                /* Create element a and print to footerNavSpan */
                document.getElementById('footerNavSpan').innerHTML += '<a id="nosotros" class="fnav__a" href="#nosotros"><label data-lang="nosotros"></label> | </a>';
            }
        })

        /* Create element a for Twitter and Facebook and append with the socialFooter*/
        document.getElementById('socialFooter').innerHTML += '<a href="'+dataCompany.twitter+'"style="text-decoration: none; color: cornflowerblue; font-size: 26px" ><i class="fab fa-twitter"></i></a>';
        document.getElementById('socialFooter').innerHTML += ' | ';
        document.getElementById('socialFooter').innerHTML += '<a href="'+dataCompany.facebook+'"style="text-decoration: none; color: darkblue; font-size: 26px" ><i class="fab fa-facebook-f"></i></a>';

        /* Create a event listener to change lang a spanish */
        document.getElementById('changeEs').addEventListener('click',() => {
            /* Save in cookies es */
            setCookies("lang","es"); 

            /* Reload the page to change the language */
            let link = Router.getFragment(window.location.href);
            if(link.split(".")[1] === undefined )
                Router.navigate(link+".");
            else
                Router.navigate(link.split(".")[0]);
        });

        /* Create a event listener to change lang a english */
        document.getElementById('changeEn').addEventListener('click',() => {
            /* Save in cookies en */
            setCookies("lang","en"); 

            /* Reload the page to change the language */
            let link = Router.getFragment(window.location.href);
            if(link.split(".")[1] === undefined )
                Router.navigate(link+".");
            else
                Router.navigate(link.split(".")[0]);
        });

        /* Create a event listener to change lang a valencian */
        document.getElementById('changeVa').addEventListener('click',() => {
            /* Save in cookies va */
            setCookies("lang","va"); 

            /* Reload the page to change the language */
            let link = Router.getFragment(window.location.href);
            if(link.split(".")[1] === undefined )
                Router.navigate(link+".");
            else
                Router.navigate(link.split(".")[0]);
        });
    }

    /**
     * Load the Header template
     * @function
     * @return {string} Return HTML header, mobile desktop and charge lodo and brackground image 
     */
    render(){
        
        return `
            <footer id="containerFooter" class="footer">
                <nav id="footerNav" class="footer__nav"> 
                    <span id="footerNavSpan"></span>
                    <a class="fnav__a" href="#contact" data-lang="Contacto"></a>
                </nav>
                <section class="footer__lang">
                    <button id="changeEs" type="button" data-lang="Es"></button>
                    <button id="changeEn" type="button" data-lang="In"></button>
                    <button id="changeVa" type="button" data-lang="Va"></button>
                </section>
                <section id="socialFooter" class="footer__section">  </section>
            </footer>
        `
    }

}

/* Export the class FooterComponent */
export default FooterComponent;