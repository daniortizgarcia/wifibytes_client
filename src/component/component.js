class Component {
    /**
     * Class Component
     * @class
     * @param {String} container 
     */
    constructor(container){
        try {
            /* Call the function render if container exist */
            if (!document.querySelector(container)) throw("Error. Selected output target for component " + this.constructor.name + " doesn't exist");
        } catch (e) {
            /* if exist print the message error in the container */
            throw e;
        }
    }
}

/* Export the class Component */
export default Component;