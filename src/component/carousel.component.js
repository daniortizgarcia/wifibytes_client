class CarouselComponent {

    constructor(){
    }

    /**
     * Load the Carousel template
     * @function 
     * @param {Array} jumbotron 
     * @return {string} Return the HTML carousel
     */
    static render(jumbotron){
        /* Declare the var where save the HTML*/
        let jumbotronHTML = "";
        let dotJumbotron = "";

        /* Map to jumbotron to charge all the slides and choose if it is image or video*/
        jumbotron.map((element,index) => {
            /* get the extension */
            let format = element.image.split(".")[element.image.split(".").length - 1];

            /* see if it is a image */
            if( format === "jpg" || format === "jpeg" || format === "png" || format === "svg" || format === "gif"){
                /* Create the HTML slider image*/
                jumbotronHTML += `
                    <section class="slide">
                        <section class="slide__pager">${index + 1}/${jumbotron.length}</section>
                        <img src="${element.image}" style=" width:100%; height:100%"/>
                        <label class="slide__text">${element.content}</label>
                    </section>
                `;

            /* see if it is a video */
            }else if(format === "mp4" || format === "mp3" || format === "ogg" || format === "webM"){
                /* Create the HTML from slider video */
                jumbotronHTML += `
                    <section class="slide" style="text-align:center; background-color: #468df8;">
                        <section class="slide__pager">${index + 1}/${jumbotron.length}</section>
                        <video controls autoplay style=" max-width:100%; height:100%">
                            <source src="${element.image}" type="video/${format}">
                        </video>
                    </section>
                `;
            }

            /* Create the dot for each slider */
            dotJumbotron += `
                <label class="dot"></label>
            `;
        })
        
        return `
            <article id="jumbotron" class="jumbotron">
                ${jumbotronHTML}
                <label id="slidePrev" class="jumbotron__prev">&#10094;</label>
                <label id="slideNext" class="jumbotron__next">&#10095;</label>
                <i id="playSlider" class="fas fa-play"></i>
                <i id="stopSlider" class="fas fa-pause"></i>
                <section class="jumbotronDot">
                    ${dotJumbotron}
                </section>
            </article>
        `;
    }
    
    /**
     * Loads carousel needs
     * @function
     */
    static startSlider() {
        this.slideIndex = 1;
        document.getElementById("playSlider").style.visibility = "hidden";
        /* Call funciton moveSlide to charge the carousel*/
        this.moveSlide(this.slideIndex);
        /* get all the class dot and add addeventlinstener to change the slide*/
        let dots =  Array.from(document.getElementsByClassName("dot"));
        dots.forEach((element,index) => {
            element.addEventListener('click',() => { this.moveSlide(this.slideIndex = index + 1)});
        });

        /* Set interval to change the slide in 5000, when click dot or prev or next stop the setInterval */
        if(!window.timeout){
            window.timeout = setInterval(() => { 
                if(window.location.href === "http://localhost:8080/#" || window.location.href === "http://localhost:8080/#home"){
                    this.moveSlide(this.slideIndex += 1);
                }
            }, 5000);
        }
        /* add addeventlistener to slidePrev to change to the previus slide*/
        document.getElementById("playSlider").addEventListener('click',() => {
            if(!window.timeout){
                this.moveSlide(this.slideIndex += 1);
                window.timeout = setInterval(() => { 
                    if(window.location.href === "http://localhost:8080/#" || window.location.href === "http://localhost:8080/#home"){
                        this.moveSlide(this.slideIndex += 1);
                    }
                }, 5000);
            }
            document.getElementById("stopSlider").style.visibility = "visible";
            document.getElementById("playSlider").style.visibility = "hidden";
        });

        /* add addeventlistener to slidePrev to change to the previus slide*/
        document.getElementById("stopSlider").addEventListener('click',() => {
            clearInterval(window.timeout); 
            delete window.timeout;
            document.getElementById("stopSlider").style.visibility = "hidden";
            document.getElementById("playSlider").style.visibility = "visible";
        });

        /* add addeventlistener to slidePrev to change to the previus slide*/
        document.getElementById("slidePrev").addEventListener('click',() => {this.moveSlide(this.slideIndex += -1)});

        /* add addeventlistener to slidePrev to change to the next slide  */
        document.getElementById("slideNext").addEventListener('click',() => {this.moveSlide(this.slideIndex += 1)});
    }

    /**
     * Move slide of the carousel
     * @function
     */
    static moveSlide(){
        /* get all the slides and dots and parse from array*/
        let slide = Array.from(document.getElementsByClassName("slide"));
        let dots =  Array.from(document.getElementsByClassName("dot"));
        
        /* when stay in the final slide and click in next go for the first slide */
        if(this.slideIndex > slide.length){
            this.slideIndex = 1;
        }

        /* when stay in the first slide and click in prev go for the final slide */
        if(this.slideIndex < 1){
            this.slideIndex = slide.length;
        }

        /* all the slides display none */
        slide.forEach(element => {
            element.style.display = "none";
        });

        /* all the dots normal class */
        dots.forEach(element => {
            element.className = element.className.replace(" active", "");
        });
        
        /* slide select display block*/
        slide[this.slideIndex-1].style.display = "block";
        
        /* dots select class activate*/
        dots[this.slideIndex-1].className += " active";
    }
}

/* Export the class CarouselComponent */
export default CarouselComponent;