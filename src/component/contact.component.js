import Component from "./component";

class ContactComponent extends Component{
    /**
     * Class ContactComponent
     * @class
     * @param {Array} data 
     * @param {String} container 
     */
    constructor(data,container){
        super(container)
        /* Call the function render if container exist */
        document.querySelector(container).innerHTML = this.render(data);

        /* when click the buttonForm call the function*/
        document.getElementById('buttonForm').addEventListener('click',() => { 
            /* Call the function validateForm*/
            let resultVal = this.validateForm();

            /* if the result of validate is true  send the email*/
            if(resultVal.valid){
                /* If the data is valid sned the email*/
                /*request('contacto','POST',resultVal.data).then((response) => {
                    console.log(response);
                })*/
            } 
        });

        /* call the function initMap to charge the map*/
        this.initMap(parseFloat(data.location_lat),parseFloat(data.location_long),data.name);
    }

    /**
     * Load the Contact template
     * @param {Array} dataContact
     * @return {string} Return HTML form, map and the company contact info
     */
    render(dataContact){
        return `
            <h1 class="contactTitle" data-lang="Contacto"></h1>
            <div id="contactContent" class="contactContent">
                <form class="contactForm">
                    <input required id="contactName" class="contactForm__input" type="text" placeholder="Name & Surname" minlength="3" maxlength="20" />
                    <label id="errorName" style="visibility:hidden;" data-lang='El nombre debe tener entre 3 y 25 caracteres'></label>
                    <input required id="contactPhone" class="contactForm__input" type="tel" placeholder="Phone" />
                    <label id="errorPhone" style="visibility:hidden;" data-lang='El teléfono no es válido'></label>
                    <input required id="contactEmail" class="contactForm__input" type="email" placeholder="Email" />
                    <label id="errorEmail" style="visibility:hidden;" data-lang='El Correo electronico no es válido'></label>
                    <textarea required row="10" id="contactMessage" class="contactForm__textarea" placeholder="Message" minlength="20" maxlength="200"></textarea>
                    <label id="errorMessage" style="visibility:hidden;" data-lang='El mensaje debe tener entre 20 y 200 caracteres'></label><br />
                    <button id="buttonForm" class="contactForm__button" type="button" data-lang="Enviar mensaje"></button>
                </form>
                <div id="map" class="map"></div>
                <div class="contactContent__infoUbi">
                    <span>${dataContact.address}, ${dataContact.city}, ${dataContact.zipcode}, ${dataContact.province}, ${dataContact.country}</span><br />
                    <span>Phone: ${dataContact.phone}</span> 
                </div>
                
            </div>
        `;
    };
    
    /**
     * Print google maps in contact
     * @function
     * @param {string} lat 
     * @param {string} long 
     * @param {string} name
     */
    initMap(lat,long,name) {
        /* Define the lat and long*/
        let myLatLng = {lat: lat, lng: long};
        
        /* The title of marker*/
        let company = name;

        /* Create the map*/
        let map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 14
        });

        /* Add marker with the Google Maps */
        new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: company
        });
    }

    /**
     * Validate form and send true or false
     * @function
     * @return {JSON} 
     */
    validateForm(){
        let errorMessage= "";

        /* Create pattern email */
        let emailPattern = new RegExp('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$');

        /* Create pattern phone*/
        let phonePattern = new RegExp("^[6|7|9][0-9]{8}$");

        /* Initialize the errors */
        document.getElementById('errorName').style.visibility = "hidden";
        document.getElementById('errorPhone').style.visibility = "hidden";
        document.getElementById('errorEmail').style.visibility = "hidden";
        document.getElementById('errorMessage').style.visibility = "hidden";

        /* Create JSON with the data*/
        let data = {
            name: document.getElementById('contactName').value,
            email: document.getElementById('contactEmail').value,
            phone: document.getElementById('contactPhone').value,
            message: document.getElementById('contactMessage').value
        }
        
        /* check the data*/
        if(data.name.length < 3 || data.name.length > 25){
            document.getElementById('errorName').style.visibility = "visible";
            errorMessage = "The name must be between 3 and 25 characters";
        }else if(data.phone.length < 1 || !phonePattern.test(data.phone)){
            document.getElementById('errorPhone').style.visibility = "visible";
            errorMessage = "The phone isn't valid";
        }else if(data.email.length < 1 || !emailPattern.test(data.email)){
            document.getElementById('errorEmail').style.visibility = "visible";
            errorMessage = "The email isn't valid";
        }else if(data.message.length < 20 || data.message.length > 200){
            document.getElementById('errorMessage').style.visibility = "visible";
            errorMessage = "The message must be between 20 and 200 characters";
        }else{
            /* If the data its true send true and data */
            return {valid:true,data:data};
        }

        /* If the data its false send false and error */
        return {valid:false,data:errorMessage};
    }

}

/* Export the class ContactComponent */
export default ContactComponent;