import Component from "./component";

class MainComponent extends Component{
    /**
     * Class MainComponent
     * @class
     * @param {Array} data 
     * @param {String} container
     * @param {String} name
     */
    constructor(data,container,name){
        super(container);
        /* Call the function render if container exist */
        document.querySelector(container).innerHTML = this.render(data,name);
    }

    /**
     * Load the Main template
     * @function
     * @param {Array} dataMain
     * @param {String} name
     * @return {string} Return the HTML main
     */
    render(dataMain,name){
        /* If dataMain length is more than 0 print main HTML else print warning message */
        if(dataMain.length > 0){
            return `
                <article class="articleMain" id="containerMain">
                    <h1 class="articleMain__title" data-lang="${name}"></h1>
                ${
                    dataMain.map( element => {
                        return `
                            <section class="articleMain__section" id="contentMain">
                                ${element.content}
                            </section>
                        `;
                    }).join('')
                }
                </article>
            `;
        }else{
            /* Return warning message */
            return `
                <article class="articleMain" id="containerMain">    
                    <label class='warning' data-lang='Elemento solicitado sin traducción, pronto estará disponible.'></label>
                </article>    
            `;
        }
        
    }
}

/* Export the class MainComponent */
export default MainComponent;