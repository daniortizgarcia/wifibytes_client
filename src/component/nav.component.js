import Component from "./component";

class HeaderComponent extends Component{
    /**
     * Class HeaderComponent
     * @class
     * @param {String} dataCompany
     * @param {String} container 
     */
    constructor(dataCompany,container){
        super(container);

        /* Call the function render if container exist */
        document.querySelector(container).innerHTML = this.render();
        
        /* Create element image to load icon with the header */
        document.getElementById("contentHeader").innerHTML += '<img src="' + dataCompany.logo + '" class="header__icon"></img>';

        dataCompany.textos.forEach( element => {
            
            if(element.key.match(/nosotros/gi) && !document.getElementById('nosotrosH') && !document.getElementById('nosotrosHV')){
                /* Create element a and print to changeNosotros */
                document.getElementById('changeNosotros').innerHTML += '<a id="nosotrosH" class="nav__a" href="#nosotros" data-lang="Nosotros"></a>';
                
                /* Create element a and print to changeNosotrosV */
                document.getElementById('changeNosotrosV').innerHTML += '<a id="nosotrosHV" class="vertical__nav__a" href="#nosotros"><label data-lang="Nosotros"></label></a>';
            }
        })
    }

    /**
     * Load the Header template
     * @function
     * @return {string} Return HTML header, mobile desktop and charge lodo and brackground image 
     */
    render(){
        
        return `
            <nav id="verticalNav" class="vertical__nav">
                <ul>
                    <li id="hideMenu" class="vertical__nav__li"><a class="vertical__nav__a"> <i class="fas fa-arrow-left"></i> </a></li>
                    <li id="changeHomeV" class="vertical__nav__li"><a class="vertical__nav__a" href="#" data-lang="Inicio"></a></li>	
                    <li id="changeTariffsV" class="vertical__nav__li"><a class="vertical__nav__a" href="#tariffs" data-lang="Tarifas"></a></li>
                    <li id="changeCatalogueV" class="vertical__nav__li"><a class="vertical__nav__a" href="#catalogue" data-lang="Catálogo"></a></li>
                    <li id="changeContactV" class="vertical__nav__li"><a class="vertical__nav__a" href="#contact" data-lang="Contacto"></a></li>
                    <li id="changeNosotrosV" class="vertical__nav__li"></li>
                </ul>
            </nav>
            <span id="showMenu" class="vertical__arrow"><i class="fas fa-bars"></i></span>
            <nav class="nav">
                <ul class="nav__ul">
                    <li class="nav__li"><a class="nav__a" href="#" data-lang="Inicio"></a></li>
                    <li class="nav__li"><a class="nav__a" href="#tariffs" data-lang="Tarifas"></a></li>
                    <li class="nav__li"><a class="nav__a" href="#catalogue" data-lang="Catálogo"></a></li>
                    <li id="contentHeader" class="header__content" style="text-align:center;"></li>
                    <li id="changeNosotros" class="nav__li"></li>
                    <li class="nav__li"><a class="nav__a" href="#contact" data-lang="Contacto"></a></li>
                </ul>
            </nav>
        `
    }

}

/* Export the class HeaderComponent */
export default HeaderComponent;