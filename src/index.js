/* imports that need all the application */
import {request,setCookies,getCookies} from './utils';
import {Router} from './router';
import {changeLang} from './i18n/translate'
import MainComponent from './component/main.component';
import CarouselComponent from './component/carousel.component';
import CatalogueComponent from './component/catalogue.component';
import ContactComponent from './component/contact.component';
import HomeComponent from './component/home.component';
import TariffsComponent from './component/tariffs.component';
import NavComponent from './component/nav.component';
import FooterComponent from './component/footer.component';

/* Router from the aplication */
Router
.add(/contact/, function() {
	request('datos_empresa').then((response) => {
		/* Call function templateContact and inner the HTML with the bodyContent*/
		new ContactComponent(JSON.parse(response),'#bodyContent')
		changeLang(getCookies("lang"));
	});
}).listen()
.add(/tariffs/, function() {
	/* Get all the tariffs */
	request('tarifa').then((response) => {
		new TariffsComponent(JSON.parse(response).results,"#bodyContent");
		changeLang(getCookies("lang"));
	});
})
.add(/catalogue/,function(){
	/* Get all the tariffs */
	request('articulo').then((responsea) => {

		let article = JSON.parse(responsea).results;
		request('familia').then((responsef) => {

			let family = JSON.parse(responsef).results
			request('filtros').then((responsefil) => {

				let filter = JSON.parse(responsefil)
				new CatalogueComponent(article,family,filter,"#bodyContent");
				changeLang(getCookies("lang"));
			});
		});	
	});
})
.add(/cookies/, function() {
	/* get data from datos_empresa */
	request('datos_empresa').then((response) => {
		let dataMain = JSON.parse(response).textos.filter(data => data.lang.includes(getCookies("lang")));
		dataMain = dataMain.filter((data) => data.key.match(/cookies/gi));
		new MainComponent(dataMain,"#bodyContent","cookies");
		changeLang(getCookies("lang"));
	});
})
.add(/legal/, function() {
	/* get data from datos_empresa */
	request('datos_empresa').then((response) => {
		let dataMain = JSON.parse(response).textos.filter(data => data.lang.includes(getCookies("lang")));
		dataMain = dataMain.filter((data) => data.key.match(/legal/gi));
		new MainComponent(dataMain,"#bodyContent","aviso legal");
		changeLang(getCookies("lang"));
	});
})
.add(/nosotros/, function() {
	/* get data from datos_empresa */
	request('datos_empresa').then((response) => {
		let dataMain = JSON.parse(response).textos.filter(data => data.lang.includes(getCookies("lang")));
		dataMain = dataMain.filter((data) => data.key.match(/nosotros/gi));
		new MainComponent(dataMain,"#bodyContent","nosotros");
		changeLang(getCookies("lang"));
	});
})
.add(function() {
	/* Get info home */
	request('home').then((response) => {
		let dataHome = [];

		/* Filter with the lang */
		dataHome.home = JSON.parse(response).filter(item => item.lang.includes(getCookies("lang")));

		/* Get the featured tariffs */
		request('tarifa?destacado=true').then((response) => {
			/* Parse the information to use later */
			dataHome.tariffs = JSON.parse(response).results;

			/* get datos_empresa */
			request('datos_empresa').then((response) => {
				let jumbotron = JSON.parse(response).textos;
				jumbotron = jumbotron.filter(data => data.key.match(/jumbotron/gi));
				dataHome.dataJumbotron = jumbotron.filter(data => data.lang.includes(getCookies("lang")));
				/* if data jumbotron is true enter a charge the jumbotron, else charge the other data*/
				if(dataHome.dataJumbotron){
					/* Call the function templateHome and inner the HTML to bodyContent */
					new HomeComponent(dataHome,"#bodyContent");
					CarouselComponent.startSlider();
				}else{
					dataHome.dataJumbotron = false;
					new HomeComponent(dataHome,"#bodyContent");
				}

				changeLang(getCookies("lang"));
			});
		});
	});
});

/* Wait for the DOMContentLoades is loaded */
document.addEventListener('DOMContentLoaded', () => {


	/* AJAX to get datos_empresa data */ 
	request('datos_empresa').then(function(response) {
		/* parse info from get datos_empresa */
		let dataCompany = JSON.parse(response);

		/* Print the Header and Footer */
		new NavComponent(dataCompany,'#headerContent');
		new FooterComponent(dataCompany,'#footerContent');

		/* Create a element link to use a icon logo and insert with the header*/
		document.getElementById('contentHead').innerHTML += '<link rel="icon" href="' + dataCompany.icon_logo + '" type="image/png">';

		/* Add the title of the page */
		document.getElementById("titlePage").innerHTML += dataCompany.name;

		/* Show and hide the menu mobile */
		document.getElementById('hideMenu').addEventListener('click',hideMenu);
		document.getElementById('showMenu').addEventListener('click',showMenu);

		if(!getCookies("lang")) setCookies("lang","es");
		changeLang(getCookies("lang"));

	}).catch(function(error) {
		console.log("Failed!", error);
	})
	
	/* Change the url to activate the home */
	Router.navigate("home");
});

/**
 * Show menu mobile
 * @function
 */
function showMenu(){
	document.getElementById('verticalNav').style.visibility = "visible"
	document.getElementById('showMenu').style.visibility = "hidden"
}

/**
 * Hide menu mobile
 * @function
 */
function hideMenu(){
	document.getElementById('verticalNav').style.visibility = "hidden"
	document.getElementById('showMenu').style.visibility = "visible"
}