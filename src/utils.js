// From Jake Archibald's Promises and Back:
// http://www.html5rocks.com/en/tutorials/es6/promises/#toc-promisifying-xmlhttprequest
import {Settings} from './settings'
/**
 * Make a request to the server
 * @function
 * @param {string} url 
 * @param {string} method 
 * @param {Array} data 
 */
function request(url,method = 'GET',data = false) {
	// Return a new promise.
	return new Promise(function(resolve, reject) {
		if (localStorage.getItem(url)) {
			resolve(localStorage.getItem(url));
		}else{
			// Do the usual XHR stuff
			var req = new XMLHttpRequest();
			req.open(method, Settings.baseURL+url);
			req.onload = function() {
				// This is called even on 404 etc
				// so check the status
				if (req.status == 200) {
					// Resolve the promise with the response text
					localStorage.setItem(url,req.response);
					resolve(req.response);
				}
				else {
				// Otherwise reject with the status text
				// which will hopefully be a meaningful error
					reject(req.statusText);
				}
			};
			// Handle network errors
			req.onerror = function() {
				reject("Network Error");
			};
			// Make the request
			if(!data){
				req.send();
			}else{
				req.send(data);
			}
		}
	});
}

/**
 * Set cookies
 * @param {String} name 
 * @param {String} value 
 * @param {Number} exdays 
 */
function setCookies(name,value = false ,exdays = 365){
	/* if value its true, set cookie */
	if(value){
		let d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		document.cookie = name + "=" + value + ";expires=" + d.toUTCString() + ';';
	}
}

/**
 * Get cookies
 * @param {String} name 
 */
function getCookies(name){
	/* Try to get lang, if value is null return false*/
	try{
		let reg = new RegExp("[\\s]*"+ name + "[\\s]*=[\\s]*([\\w]*)");
		return document.cookie.match(reg)[1];
	}catch(e){
		return false;
	}
}	

/**
 * Delete cookies
 * @param {String} name 
 */
function delCookies(name){
	/* Delete the cookies */
	document.cookie = name +'=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

/* Export the request, setCookies, getCookies, delCookies */
export {request,setCookies,getCookies,delCookies};